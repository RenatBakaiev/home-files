
//----------------------------Экземпляр Vue----------------------------------------------

// var data = { a: 1 }

// var vm = new Vue({
//   data: data
// })

//--------------------------------------------

var obj = {
    foo: 'bar'
  }
  
//   Object.freeze(obj)
  
  new Vue({
    el: '#app',
    data: obj
  })

//--------------------------------------------

var data = { a: 1 }

var vm = new Vue({
  el: '#example',
  data: data
})

vm.$watch('a', function (newValue, oldValue) {
    console.log('Этот коллбэк будет вызван, когда изменится `vm.a`');
  })

//---------------------------------------------------------------------

var data_example = { test: 'hello'};

var vm_test = new Vue({
    el: '#test_example',
    data: data_example,
})

vm_test.$watch('test', function (newValue, oldValue) {
    console.log('Изменился vm_tast.test');
  })

//--------------------------хук created---------------------------------------

new Vue({
    data: { a: 1 },
    created: function(){
        console.log(data.a, ' = ЗНАЧЕНИЕ a =', this.a);
    }
})


















