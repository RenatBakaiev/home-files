import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';

import router from './router'
import store from './store'

axios.defaults.baseURL = 'https://vue-axios-project-e52b6.firebaseio.com';
axios.defaults.headers.common['Authorization'] = 'something';
axios.defaults.headers.get['Accepts'] = 'application/json';

const reqInterceptor = axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config); // задать параметры запросу
  return config
});

const resInterceptor = axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res); // задать параметры ответу
  return res
});

axios.interceptors.request.eject(reqInterceptor); // удалить перехватчик
axios.interceptors.response.eject(resInterceptor); // удалить перехватчик


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
