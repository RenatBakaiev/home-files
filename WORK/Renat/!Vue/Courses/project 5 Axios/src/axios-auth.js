import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://vue-axios-project-e52b6.firebaseio.com'
});

instance.defaults.headers.common['SOMETHING'] = 'something';

export default instance
