import VueRouter from 'vue-router'
import Vue from 'vue'
import ExampleRouterEntry from './components/ExampleRouterEntry.vue'
import User from './components/User.vue'
import Main from './components/Main.vue'
import HelloWorld from './components/HelloWorld'
import Example from './components/Example'
import ModuleRouter from './module/router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Main
  },
  {
    path: '/example',
    component: ExampleRouterEntry,
    children: [
      {
        path: '',
        component: Example
      },
      {
        path: 'test',
        name: 'super-puper',
        component: HelloWorld
      }
    ]
  },
  {
    path: '/user/:userId',
    component: User
  },
  ModuleRouter
]

export default new VueRouter({ routes })
