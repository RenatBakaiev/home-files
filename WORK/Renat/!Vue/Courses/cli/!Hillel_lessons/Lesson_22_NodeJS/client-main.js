// // console.log('hi browser');
// // СОЗДАНИЕ
// var xhr = new XMLHttpRequest();
//
// // НАВЕСИЛИ ОБРАБОТЧИК СОБЫТИЯ readystatechange - состояние запроса от 0 до 4
// xhr.addEventListener('readystatechange', function () {
//   // console.log(xhr);
//   if (xhr.readyState === 4){      // 4 - запрос закончен с успешным/неуспешным статусом,
//     // console.log(xhr);          // 1 - ничего не началось
//     example(xhr)
//   }
// });
//
// // КОНФИГУРАЦИЯ 2 - сконфигурировали
// xhr.open('GET', '/info/user.json', true); //открытие канала - и указание типа запроса GET (POST, PUT, DELETE)
//
//
//
// function example (xhr) {
//   var response = xhr.responseText;
//   var parsedReps = JSON.parse(response); //JSON преобразует из JSON в объект
//   console.log(parsedReps); // получаем норм JS объект
// }
//
// // fetch();
//
// // ВЫПОЛНЕНИЕ ЗАПРОСА
// xhr.send(); //запрос послать - корабль пошёл) 3 - отправили данные и пакеты приходят

// ---------------------------------------------------------------------------------------
// LECTION 23

// ПЕРЕЗАПИСЬ ВСЕГО

// request('GET', '/info/user.json')

function success (data) {
  console.log(data);
  if(data && data.user && data.user.friends){
    return data.user.friends
  }
  return []
}

// отрисовка списка
function render (friends) {
  let template = `
    <div id="container">
        <ul>
            ${friends.map(function( value ){ return `<li>${value}</li>`}).join('')}
        </ul>
    </div>
  `;


  let container = document.querySelector('#container');
  container && container.remove();

  document.body.innerHTML += template;
}

function request (method, path) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('readystatechange', function () { // это callback
      if (xhr.readyState === 4){
        // console.log(xhr);
        success(xhr) // это второй callback
      }
    });

    xhr.open(method, path, true);
    xhr.send();
}

// PROMISE

// var userData = new Promise(function(resolve, reject){ // Обещание или дело
//   setTimeout(function () {
//     var x = Math.floor(Math.random()*10) - 5;
//     console.log('TIMER 1 sec', x);
//     if(x > 0){
//       resolve(x);
//     }
//     reject(x);
//   }, 1000)
// });
// // console.log(userData);
//
// userData                               // Когда оно выполнится делаем действие одно или другое
//   .then(
//       function (resp) {
//         console.log('THEN RESOLVE', resp);
//       },
//       function (error) {
//         console.log('THEN REJECT', error);
//       }
//       )
//
// userData.then(function (resp) {
//   console.log(resp)
// })

function filterByNameKatya (friends) {
  return friends.filter(function (fr) {
    return fr === 'Katya'
  })
}

// AJAX WITH PROMISE

var method = "GET", path = '/info/user.json';

var async = new Promise(function (resolve, reject) {
  var xhr = new XMLHttpRequest();
  xhr.addEventListener('readystatechange', function () {
    if (xhr.readyState === 4){
      if(xhr.status !== 200){
        reject();
        return
      }
      resolve(JSON.parse(xhr.responseText));
    }
  });
  xhr.open(method, path, true);
  xhr.send();
})

async
  .then(success)
  .then(filterByNameKatya)
  .then(render)
  .catch(function (error) {
    console.error('ERROR: ', error)
  })

//-------------------------------------------------------------------------------------------













