// LECTION 23 PROMISE basic -> in LECTION 22 NodeJs + Ajax
// LECTION 24 PROMISE ASYNC AWAIT

function getAsyncFunc (timer = 1000) {
  let async = new Promise(function (resolve, reject) {
    console.log(`Timer start: ${timer}`)
    setTimeout(function () {
      console.log(`Timer end: ${timer}`)
      resolve(timer)
      // reject(200)
    }, timer)
  });
  return async
}

// getAsyncFunc ()
//   .then(function (value) {
//     console.log('Resolve: ', 2, 'Value :', value)
//
//     return value/10 // return имеет смысл если мы хотим что-то пробросить в следующий then
//   }, function (value) {
//     console.log('Reject: ', 2, 'Value :', value)

    // return value*10
    // return Promise.reject(value*10) === 27 - лучше применить если нет асинхронных операций

    // return new Promise(function (resolve, reject) {
    //   setTimeout(function () {
    //     reject(value*10)
    //   }, 2000)
    // });

  //   return getAsyncFunc(2000)
  // })
  // .then(function (value) {
  //   console.log('Resolve: ', 3, 'Value :', value)
  // }, function (value) {
  //   console.log('Reject: ', 3, 'Value :', value)
  // })
//-------------------------------------------------------------------------------------------
//   .finally(function () {
//     console.log('Final result') // выполняется когда все then закончаться
//   })

// getFile('/data_1.json')
//     .then(function(resp){
//
//         getFile('/data_2.json')
//             .then(function(resp2){
//                 console.log(resp.concat(resp2));
//             });
//     })
// .then(function(resp2){
//     console.log(resp.concat(resp2));
// });

Promise                            // Когда все промисы закончат выполнение - запуститься then
  .all([                            // Хоть 1 - ошибка then не запустится
        getAsyncFunc(1000),
        getAsyncFunc(3000),
        getAsyncFunc(100),
        getAsyncFunc(5000),
  ])
    .then(function (list) {
    console.log(list)
  })
  .catch(function (error) {
    console.error(error)
  })


// ASYNC / AWAIT

async function getAllData () {
    let d1 = await getFile('/data_1.json'); // запускается промис, пока не даст ответ не пойдет дальше!
    console.log('1')
    let d2 = await getFile('/data_2.json');
    let d3 = await getFile('/data_3.json');

//     Promise.all([d1, d2, d3]).then() // делали раньше 10 лет сейчас не нужно так делать
//       return d1.then((r1) => {
//         return d2.then((r2) => {
//           return d3.then((r3) => {
//             r3.concat(r1.concat(r2));
//           })
//         })
//       })
    return d3.concat(d1.concat(d2));
}

let data = getAllData();
// getAllData().then(data => {
//     console.log(data);
// })














