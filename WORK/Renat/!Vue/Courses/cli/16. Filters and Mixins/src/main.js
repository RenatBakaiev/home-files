import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', function(value){         // global filter
  return value.toLowerCase();
});

Vue.mixin({                                                // global mixin
  created() {
    console.log('Global Mixin - Created Hook');
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
