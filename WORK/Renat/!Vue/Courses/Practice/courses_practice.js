// Courses_practice VUe js

// practice 1

// new Vue({
//     el: '#exercise',
//     data: {
//         name: 'Renat',
//         age: 33,
//         image: 'https://www.google.com/search?q=vue+js&sxsrf=ALeKk03csEgYbplI9T-0F5Se5LFNnHgUAQ:1585916721944&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjeguuWoMzoAhVPaJoKHXvNDGEQ_AUoAnoECBQQBA&biw=1167&bih=768#imgrc=S3z7yPQSQ6oGDM'
//     },
//     methods: {
//         random: function(){
//             return Math.random();
//         }
//     }
// })

// practice 2

// new Vue({
//     el: '#exercise',
//     data: {
//         value: '',
//     },
//     methods: {
//         showAlert(){
//             alert('Hello!')
//         }
//     }
// });

// practice 3

// new Vue({
//     el: '#exercise',
//     data: {
//         value: 0
//     },
//     computed: {
//         result: function(){
//             return this.value == 25 ? 'done' : 'not there yet'
//         }
//     },
//     watch: {
//         result: function() {
//             var vm = this;
//             setTimeout(function(){
//                 vm.value = 0;
//             }, 5000);
//         }
//     }
// });

// practice 4
// new Vue({
//     el: '#exercise',
//     data: {
//         effectClasses: {
//             hightlight: false,
//             shrink: true,
//         },
//         float: 'float',
//         userClass: '',
//         isVisible: true,
//         myStyle: {
//             width: '100px',
//             height: '150px',
//             backgroundColor: 'gray',
//         },
//         progressBar: {
//             width: '0px',
//             backgroundColor: 'red',
//         }
//     },
//     methods: {
//         startEffect: function(){
//             var vm = this;
//             setInterval( function(){
//                 vm.effectClasses.hightlight = !vm.effectClasses.hightlight;
//                 vm.effectClasses.shrink = !vm.effectClasses.shrink;
//             }, 1000)
//         },
//         startProgress: function(){
//             var vm = this;
//             var width = 0;
//
//             setInterval(function(){
//                 width = width + 10;
//                 vm.progressBar.width = width + 'px';
//             }, 500)
//         }
//     }
// });

// practice 5 array object

// new Vue({
//     el: '#exercise',
//     data: {
//         isShown: true,
//         array: ['Renat','Vita', 'Renata', 'Rehina'],
//         myObject: {
//             title: 'Lord of the Rings',
//             author: 'J.R.R Tolkiens',
//             books: '3'
//         },
//         testData: {
//             name: 'TESTOBJECT',
//             id: 10,
//             data: [1.67, 1.33, 0.98, 2.21]
//         }
//     }
//
// });



























