// Courses VU js
// 1.
// new Vue({
//     el:'#app',
//     data: {
//         title: 'Hello Renat',
//         link: 'http://google.com',
//         finishedLink: '<a href="http://google.com">Google<a>',
//     },
//     methods:{
//         changeTitle: function(){
//             this.title = event.target.value;
//         },
//         sayHello: function(){
//             this.title = 'Hello!'
//             return this.title;
//         }
//
//     }
// });

// 2.
// new Vue({
//     el:'#app',
//     data: {
//         counter: 0,
//         x: 0,
//         y: 0,
//     },
//     methods: {
//         increase: function(step, event){
//             this.counter += step;
//         },
//         updateCoordinates: function(event){
//             this.x = event.clientX;
//             this.y = event.clientY;
//         },
//         /*   dummy: function(eveny){
//             event.stopPropagation();
//           } */
//         alertMe: function(){
//             alert('Alert!');
//         },
//     }
// });

// 3. 21 lesson
// new Vue({
//     el:'#app',
//     data: {
//         name: 'Max'
//     }
// });

// 4. lesson 23 make counter 0 2 sec
// new Vue({
//     el:'#app',
//     data: {
//         counter: 0,
//         secondCounter: 0,
//     },
//     computed:{
//         output: function(){
//             console.log('Computed');
//             return this.counter > 5 ? 'Greater 5' : 'Smaller 5'
//         }
//     },
//     watch: {
//         counter: function(value){
//             var vm = this;
//             setTimeout(function(){
//                 vm.counter = 0;
//             },2000);
//         }
//     },
//     methods:{
//         result: function(){
//             console.log('Method');
//             return this.counter > 5 ? 'Greater 5' : 'Smaller 5'
//         }
//     }
// });

// 5. lesson 24 v-on: === @, v-bind: === :
// new Vue({
//     el:'#app',
//     data: {
//         link: 'http://google.com'
//     },
//     methods: {
//         changeLink: function(){
//             this.link = 'http://apple.com'
//         }
//     }
// });

// 6. Change colors divs - lesson 27
// new Vue({
//     el:'#app',
//     data: {
//         attachRed: false,
//         color: 'green',
//     },
//     computed: {
//         divClasses: function(){
//             return {
//                 red: this.attachRed,
//                 blue: !this.attachRed,
//             }
//         }
//     }
// });

// 7. Styles Dynamically without css (height weight) lesson 29

// new Vue({
//     el:'#app',
//     data: {
//         color: 'gray',
//         width: 100
//     },
//     computed: {
//         myStyle: function(){
//             return {
//                 backgroundColor: this.color,
//                 width: this.width + 'px',
//             }
//         }
//     }
// });

// 8. v-if v-else v-show lesson 34
// new Vue({
//     el:'#app',
//     data: {
//         show: true,
//     },
// });

// 9. v-for [] {} :key lesson 39

// new Vue({
//     el:'#app',
//     data: {
//         ingredients: ['meat', 'fruit', 'cookies'],
//         persons: [
//             {name: 'Max', age: '27', color: 'red'},
//             {name: 'Anna', age: 'unknown', color: 'blue'},
//         ]
//     },
// });

// -------------------------------INSTANCE------------------------------------

// 10 access to app1 ref lesson 63

// var data = {
//     title: 'The VueJS Instance',
//     showParagraph: false,
// }
//
// var vm1 = new Vue({
//     el:'#app1',
//     data: data,
//     methods: {
//         show: function(){
//             this.showParagraph = true;
//             this.updateTitle('The VueJS Instance (Updated)');
//             this.$refs.myButton.innerText = 'Test';
//         },
//         updateTitle: function(title){
//             this.title = title;
//         }
//     },
//     computed: {
//         lowercaseTitle: function(){
//             return this.title.toLowerCase()
//         }
//     },
//     watch: {
//         title: function(value){
//             alert('Title changed, new value: ' + value);
//         }
//     }
// });
//
// console.log(vm1.$data === data);
// // vm1.$refs.heading.innerText = 'Something else';
//
// setTimeout( function(){
//     vm1.title = 'Changed by Timer';
//     vm1.show();
// },3000)
//
// var vm2 = new Vue({
//     el: '#app2',
//     data: {
//         title: 'The second instance'
//     },
//     methods: {
//         onChange: function(){
//             vm1.title = 'Changed!';
//         }
//     }
// })
//
// 11. Create and using component

// var data = {
//     title: 'The VueJS Instance',
//     showParagraph: false,
// }
//
// Vue.component('hello', {
//     template: '<h1>Hello!</h1>' //                                   CREATE COMPONENT
// });
//
// var vm1 = new Vue({
//     data: data,
//     methods: {
//         show: function(){
//             this.showParagraph = true;
//             this.updateTitle('The VueJS Instance (Updated)');
//             this.$refs.myButton.innerText = 'Test';
//         },
//         updateTitle: function(title){
//             this.title = title;
//         }
//     },
//     computed: {
//         lowercaseTitle: function(){
//             return this.title.toLowerCase()
//         }
//     },
//     watch: {
//         title: function(value){
//             alert('Title changed, new value: ' + value);
//         }
//     }
// });
//
// vm1.$mount('#app1');
//
// console.log(vm1.$data === data);
// vm1.$refs.heading.innerText = 'Something else';
//
// setTimeout( function(){
//     vm1.title = 'Changed by Timer';
//     vm1.show();
// },3000)
//
// var vm2 = new Vue({
//     el: '#app2',
//     data: {
//         title: 'The second instance'
//     },
//     methods: {
//         onChange: function(){
//             vm1.title = 'Changed!';
//         }
//     }
// });
//
// var vm3 = new Vue({
//     template: '<h1>Hello!</h1>'
// });
//
// vm3.$mount();
// document.getElementById('app3').appendChild(vm3.$el);


// 12. Life cycle Vue instance
// new Vue({
//     el: '#app',
//     data: {
//         title: 'The VueJS Instance',
//     },
//     beforeCreate: function(){
//         console.log('beforeCreate()');
//     },
//     created: function(){
//         console.log('created()');
//     },
//     beforeMount: function(){
//         console.log('beforeMount()');
//     },
//     mounted: function(){
//         console.log('mounted()');
//     },
//     beforeUpdate: function(){
//         console.log('beforeUpdate()');
//     },
//     updated: function(){
//         console.log('updated()');
//     },
//     beforeDestroy: function(){
//         console.log('beforeDestroy()');
//     },
//     destroyed: function(){
//         console.log('destroyed()');
//     },
//     methods: {
//         destroy: function(){
//             this.$destroy();
//         }
//     }
// });

// 13.Component

// Vue.component('my-cmp',{
//     data: function() {
//         return {
//             status: 'Critical'
//         }
//     },
//     template: '<p>Server Status: {{ status }}</p>'
// });
//
// new Vue({
//     el: '#app',
// })

//14. Components different this for buttons lesson 84

//var data = { status: 'Critical'};                   // one this for buttons

// Vue.component('my-cmp',{
//     data: function() {
//         //return data                                 // one this for buttons
//         return {
//             status: 'Critical'                        // different this for buttons
//         };
//     },
//     template: '<p>Server Status: {{ status }} (<button@click="changeStatus">Change</button>)</p>',
//     methods: {
//         changeStatus: function(){
//             this.status = 'Normal';
//         }
//     }
// });
//
// new Vue({
//     el: '#app',
// })

// 15. Registering component globally and locally lesson 85

//var data = { status: 'Critical'};

//Vue.component('my-cmp', {})                                  // GLOBALLY
// var cmp = {
//     data: function() {
//         return {
//             status: 'Critical'
//         };
//     },
//     template: '<p>Server Status: {{ status }} (<button@click="changeStatus">Change</button>)</p>',
//     methods: {
//         changeStatus: function(){
//             this.status = 'Normal';
//         }
//     }
// };
//
// new Vue({
//     el: '#app',
//     components: {
//         'my-cmp': cmp                                        // LOCALLY
//     }
// })
//
// new Vue({
//     el: '#app2',
// })










