// var app = new Vue({ //вызываем и порождаем экземпляр - аргументы - 1 объект с 2 ключами
//   el: '#app', // ссылка на элемент - селектор в котором создается приложение
//   data: { // данные (model) которые доступны в div
//     x: 200,
//     y: 300,
//     list: [1, 2, 3, 4],
//     info: {}
//   }
// })

// console.log(app);

// setTimeout(() => {
//   console.log(app.message);
//   app.message = 600; // в объект добавили ключ message не динамически
//   Vue.set(app.info, 'message', 600); // добавление в app info - ключ со значением 600
// }, 1500)

//-------------------------------------app2-----------------------------------------

// var app2 = new Vue({
//   el: '#app-2',
//   data: {
//     className: 'active',
//     appearence: 'transition: .5s; color: green;',
//     value: 10,
//     message: 'Вы загрузили эту страницу в: ' + new Date().toLocaleString()
//   }
// })

// // res = appearence  + 'font-size:' + value*3 + 'px'; //сформировали выражение - скопировали 
// //                                                    // с html

// setInterval(() => {
//  app2.value = Math.floor(Math.random() * 60)
// }, 1000);

//--------------------------------------app3-----------------------------------------

// var app3 = new Vue({
//   el: '#app-3',
//   data: {
//     seen: true,
//     info: 'Hello Info!'
//   }
// })

// setTimeout(() => {
//   app3.seen = false;
// }, 2000)

//--------------------------------------app4-----------------------------------------

// var app4 = new Vue({
//   el: '#app-4',
//   data: {
//     arr: [
//       1, 8, 9, 6
//     ],
//     item: [1, 2],
//     list: [
//       {
//         text: 'Изучить JavaScript'
//       },
//       {
//         title: 'jasnf'
//       },
//       {
//         text: ''
//       },
//       {
//         text: 'Создать что-нибудь классное'
//       },
//       {
//         text: 'Создать что-нибудь классное'
//       },
//       {
//         text: 'Создать что-нибудь классное'
//       },
//       {
//         text: 'Создать что-нибудь классноее'
//       },
//       {
//         text: 'Создать что-нибудь классноеее'
//       }
//     ],
//     info: {
//       x: 1,
//       y: 3,
//       z: 5,
//       p: 6,
//       l: 9
//     },
//     renat_family: {
//       '1': 'Renat',
//       '2': 'Vita',
//       '3': 'Renata',
//       '4': 'Rehina',
//     }
//   }
// })

//--------------------------------------app5-----------------------------------------

// var app5 = new Vue({
//   el: '#app-5',
//   data: {
//     message: 'Привет, Vue.js!',
//     counter: 3,
//     list: [6, 9, 18],
//     specialNames: [
//       "super",
//       "puper",
//       "trooper",
//       "cooper",
//       "dooper"
//     ],
//     classNames: []
//   },
//   methods: { //функции сохраняются в методах
//     calculate(value) {
//       return `<sup>${value*10}</sup>`;
//     },
//     onClick(event, id) {
//       // console.log('clicked', arguments, id);
//       this.counter++;
//     },
//     addRandClass() {
//       let special = this.specialNames[Math.floor(Math.random()*this.specialNames.length)];
//       console.log(special);
//       this.classNames.push(special);
//     }
//   }
// })

//--------------------------------------app6-----------------------------------------

// var app6 = new Vue({
//   el: '#app-6',
//   data: {
//     message: 'Привет, Vue!!!',
//     result: 'RESULT',
//     x: 10,
//     switcher: false,
//     date: ''
//   },
//   methods: {
//     onInput(event) {
//       this.message = event.target.value
//     }
//   }
// })

//-------------------------------LIFE CYCLE---------------------------------------

// var appData = {
//   x: 60,
//   y: 200,
//   example: 'text',
//   inner: {
//     data: 120
//   }
// }
//
// // console.log(appData)
// // debugger;
// var app = new Vue({
//   el: '#app',
//   data: appData,
//   methods: {
//     onClick(){},
//     getCustomValue(){
//       return this.x+this.y + this.inner.z;
//     }
//   },
  // create hooks

  // beforeCreate(){
  //   console.log('before created');
  //   // debugger;
  // },
  // created(){
  //   console.log('created');
    // console.log(this);
    // debugger;
  // },
  // beforeMount(){
  //   console.log('before mount')
  //   debugger;
  // },
  // mounted(){
  //   console.log('mounted')
  //   debugger;
  // },
  // beforeUpdate(){
  //   console.log('before updated')
  //   debugger;
  // },
  // updated(){
  //   console.log('updated');
  //   debugger;
  // },
  // beforeDestroy(){
  //   console.log('before destroy')
  //   // debugger;
  // },
  // destroyed(){
  //   console.log('destroy')
  //   // debugger;
  // }
// })

// console.log(appData);
// appData.inner.z = 1000;
// Vue.set(appData.inner, 'z', 1000);
// app.$destroy();

//-------------------------------COMPONENT---------------------------------------

Vue.component('front-end',{
  // props: ['test', 'numbers'],
  props: {
    test: {
      type: [Number, String],
    },
    numbers: {
      type: Array,
      default: [6,9,0],
    },
    third: {
      type: Boolean,
      default: true,
    }
  },
  data(){
    return { title: 'Hello Renat Vue-master!'}
  },
  methods: {
    onPush(){
      this.title += '+';
    },
    Remove(key){
      console.log(key)
      this.numbers.splice(key, 1);
    },
  },
  template: `
         <div class="example">
             <h2>{{title}}</h2>
             <h3>{{test}}</h3>
             <button @click="onPush">Click</button>
             <input type="text" v-model="test">
             <hr>
             <ol>
                <li v-for="(item, key) in numbers" @click="Remove(key)">
                Item {{item}}
                </li>
             </ol>
         </div>
     `,
  mounted(){
    console.log(this);
  },
});

Vue.component('front-button',{
  props: ['type', 'text', 'onClick'],
  methods: {
    onEventClick(){
      this.onClick && this.onClick();
    }
  },
  template: `
        <button class="button" :class="type" @click="onEventClick">
        {{ text }}
        </button>
     `,
  mounted(){
    console.log(this);
  },
});

let app = new Vue({
  el: '.app',
  data: {
    x: 10,
    title: 'Hello world!',
    checker: false,
    testApp: 'testText',
    list: [1,6,8,9],
  },
methods:{
    onClickFrontBtnPrimary(){
      console.log('onClickFrontBtnPrimary')
    },
    onAddtoCart(){
      console.log('add to cart')
      console.log('POST')
    },
},
});







// Vue.component('front-end', {
//   // props: ['example', 'numbers'],
//   props: {
//     example: {
//       type: [Number, String]
//     },
//     numbers: {
//       type: Array,
//       default: [6, 9, 0]
//     },
//     third: {
//       type: Boolean,
//       default: true
//     }
//   },
//   data(){
//     return {
//       title: "Hello Front end!"
//     }
//   },
//   methods: {
//     onClick() {
//       this.title += "!"
//     },
//     Remove(key) {
//       this.numbers.splice(key, 1);
//     }
//   },
//   template: `
//         <div class="example">
//             <h2>{{title}}</h2>
//             <h3>{{example}}</h3>
//             <button @click="onClick">Click</button>
//             <input type="text" v-model="example">
//             <hr>
//             <ol>
//                 <li v-for="(item, key) in numbers" @click="Remove(key)">
//                     Item {{item}}
//                 </li>
//             </ol>
//         </div>
//     `,
//   mounted() {
//     console.log(this);
//   }
// });
//
// Vue.component('front-button', {
//   props: ['type', 'text', 'onClick'],
//   methods: {
//     onEventClick() {
//       this.onClick && this.onClick();
//     }
//   },
//   template: `
//         <button class="button" :class="type" @click="onEventClick">
//             {{ text }}
//         </button>
//     `,
//   mounted() {
//     console.log(this);
//   }
// });
//
// let app = new Vue({
//   el: '.app',
//   data: {
//     x: 10,
//     title: 'Hello world!',
//     checker: false,
//     exampleApp: 'Hello Front End!',
//     list: [1, 6, 7, 8, 9]
//   },
//   methods: {
//     onClickFrontBtnPrimary() {
//       console.log('onClickFrontBtnPrimary');
//     },
//     onAddToCart() {
//       console.log('add to cart');
//
//       console.log('POST', this.list);
//     }
//   },
//   beforeMount(){
//     // debugger;
//   }
// });


















