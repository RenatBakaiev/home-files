//__________________________________________TASK 10_________________________________________________________________

//1. Сделайте функцию, которая возвращает куб числа. Число передается параметром.

let func1 = (n, s) => n ** s;

console.log("task 10.1 :", func1(3, 3));

//------------------------------------------------------------------------------------------------------------------

//2. Сделайте функцию, которая возвращает квадрат числа. Число передается параметром.

let func2 = (n2, s2) => n2 ** s2;

console.log("task 10.2 :", func1(5, 2));

//------------------------------------------------------------------------------------------------------------------

//3. Сделайте функцию, которая возвращает сумму двух чисел.

let func3 = (n3, s3) => n3 + s3;

console.log("task 10.3 :", func3(8, 4));

//------------------------------------------------------------------------------------------------------------------

//4. Сделайте функцию, которая отнимает от первого числа второе и делит на третье.

let func4 = (n4, s4, l4) => (n4 - s4) / l4;

console.log("task 10.4 :", func4(100, 50, 5));

//------------------------------------------------------------------------------------------------------------------

//5. Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на русском языке.


// bad variant
// let a = 5;
// switch (a) {
//   case 1:
//   console.log('Понедельник')    
//     break;
//   case 2:
//   console.log('Вторник')    
//     break;
//   case 3:
//   console.log('Среда')    
//     break;
//   case 4:
//   console.log('Четверг')    
//     break;
//   case 5:
//   console.log('Пятница')    
//     break;
//   case 6:
//   console.log('Суббота')    
//     break;
//   case 7:
//   console.log('Воскресенье')    
//     break;
// };

let week = {
  1: 'Понедельник',
  2: 'Вторник', 
  3: 'Среда', 
  4: 'Четверг', 
  5: 'Пятница', 
  6: 'Суббота', 
  7: 'Воскресенье', 
};

function getDayOfWeek(weekday){
  console.log("task 10.5 :", week[weekday]);
}

let n = prompt('Введите число от 1 до 7 и получите день недели:');
getDayOfWeek(n);

//------------------------------------------------------------------------------------------------------------------