// JS

//_______________________________Recursion________________________________________________

//Задача: выполнения вычисления a в степени b;
function recursion(a,b){
    // debugger;
    if (b == 1){
        return a;
    }else{
        return a * recursion(a, b - 1);
    }
};

console.log('RECURSION :', recursion(3,4));

// ИЛИ

let recursion2 = (a,b) => b == 1 ? a : (a * recursion2(a, b-1));

console.log('RECURSION ARROW FUNCTION :', recursion2(3,4));

//--------------------------------------------------------------------------------------

/*Task 1. Напишите программу на JavaScript для вычисления факториала числа. 
В математике факториал неотрицательного целого числа n, обозначенного через n !,
является произведением всех натуральных чисел, меньших или равных n. 
Например, 5! = 5 x 4 x 3 x 2 x 1 = 120*/

function factorial(f){
    if (f == 1){
        return 1;
    }else{
        return f * factorial(f-1);
    }
};
console.log('RECURSION TASK1:', factorial(5));

//--------------------------------------------------------------------------------------

/*Task 2. Напишите программу на JavaScript, 
чтобы найти наибольший общий делитель (gcd) из двух положительных чисел.*/

function gcb(a,b){
    if(!b){
        return a
    }else{
        return gcb(b, a % b);
    };
};

console.log('RECURSION TASK2:', gcb(8,2));

//--------------------------------------------------------------------------------------

/*Task 3. Напишите программу на JavaScript, чтобы получить целые числа в диапазоне (x, y). 
Пример : диапазон (2, 9) Ожидаемые результаты: [3, 4, 5, 6, 7, 8].*/

function wholeNumbers(start, end){
    if(end - start === 2){
        return [start + 1];
    }else{
        var res = wholeNumbers(start, end - 1);
        res.push(end - 1);
        return res;
    }
};

console.log('RECURSION TASK3:', wholeNumbers(2,9));


//--------------------------------------------------------------------------------------

/*Task 4. Напишите программу на JavaScript, чтобы вычислить сумму массива целых чисел. 
Пример: var array = [1, 2, 3, 4, 5, 6] Ожидаемый результат: 21*/

function sumNumbers(a){
    if(a == 1){
        return a;
    }else{
        return a + sumNumbers(a - 1);
    }
};

console.log('RECURSION TASK4:', sumNumbers(6)); //11441 max

//--------------------------------------------------------------------------------------

/*Task 5. Напишите программу на JavaScript для вычисления показателя степени числа. 
Примечание. Показатель числа говорит о том, сколько раз базовое 
число используется как фактор. 8 2 = 8 x 8 = 64. Здесь 8 - основание, а 2 - 
показатель степени.*/

function stepen(a,b){
    if (b == 1){
        return a;
    }else{
        return a * stepen(a, b - 1);
    }
};

console.log('RECURSION TASK5:', stepen(8,2));

// ИЛИ

let stepen2 = (a,b) => b == 1 ? a : (a * stepen2(a, b-1));

console.log('RECURSION TASK5 var2:', stepen2(8,2));

//--------------------------------------------------------------------------------------

/*Task 6. Напишите программу на JavaScript, чтобы получить первые n чисел Фибоначчи. 
Примечание. Последовательность Фибоначчи - это последовательность чисел: 
0, 1, 1, 2, 3, 5, 8, 13, 21, 34
Каждое последующее число является суммой двух предыдущих.*/

function firstNumbersOfFibonacci(num){
    if(num === 1){
        return [0, 1];
    }else{
        var f = firstNumbersOfFibonacci(num - 1);
        f.push(f[f.length - 1] + f[f.length - 2]);
        return f;
    }
};

console.log('RECURSION TASK6:', firstNumbersOfFibonacci(3));

//--------------------------------------------------------------------------------------

/*Task 7. Напишите программу на JavaScript, чтобы проверить, 
является ли число четным или нет.*/

function parity(number){
    if (number < 0){
        number = Math.abs(number);
    }
    if (number === 0){
        return true;
    }
    if (number === 1){
        return false;
    }else{
        number = number - 2;
        return parity(number);
    }
};

console.log('RECURSION TASK7:', parity(10));

//--------------------------------------------------------------------------------------

//__________________________________JSON________________________________________________

let listOfCountries = [
    {
        "id":1,
        "name":"Країни",
        "children":[
            {
                "id":11,
                "name":"СНД (склад починаючи з 2009 року)",
                "children":[
                    {
                        "id":111,
                        "name":"Митний Союз(склад починаючи з 2015 року)",
                        "children":[
                            {
                                "id":2,
                                "name":"Росія",
                            },
                            {
                                "id":21,
                                "name":"Вірменія",
                            },
                            {
                                "id":22,
                                "name":"Білорусь",
                            },
                            {
                                "id":23,
                                "name":"Казахстан",
                            },
                        ]
                    },
                    {
                        "id":222,
                        "name":"Інші країни СНД",
                        "children":[
                            {
                                "id":31,
                                "name":"Азербайджан",
                            },
                            {
                                "id":32,
                                "name":"Таджикистан",
                            },
                            {
                                "id":45,
                                "name":"Молдова",
                            }
                        ]
                    }
                ]
            },
            {
                "id":56,
                "name":"ЄС (склад починаючи з 2013 року)",
                "children":[
                    {
                        "id":58,
                        "name":"Австрія",
                    },
                    {
                        "id":70,
                        "name":"Бельгія",
                    },
                    {
                        "id":95,
                        "name":"Болгарія",
                    },
                    {
                        "id":94,
                        "name":"Кіпр",
                    },
                    {
                        "id":205,
                        "name":"Чехія",
                    }
                ]
            }
        ]
    },
]
