//__________________________________________TASK 14_________________________________________________________________

//1. Заполните массив следующим образом: в первый элемент запишите 'x', во второй 'xx', в третий 'xxx' и так далее.

let arr1 = [];
let str1 = '';
for(let i = 0; i < 5; i++) {
  str1 = str1 + 'z'
  arr1.push(str1);
}
console.log("task 14.1 :", arr1);

//------------------------------------------------------------------------------------------------------------------

//2. Заполните массив следующим образом: в первый элемент запишите '1', во второй '22', в третий '333' и так далее.

function fillArr(){
  let arr = [];
  for(let i = 1; i < 6; i++) {
      arr.push(i.toString().repeat(i));
  }
  console.log("task 14.2 :", arr);
};
fillArr();

//------------------------------------------------------------------------------------------------------------------

/*3. Сделайте функцию arrayFill, которая будет заполнять массив заданными значениями. 
Первым параметром функция принимает значение, которым заполнять массив, а вторым - сколько элементов 
должно быть в массиве. Пример: arrayFill('x', 5) сделает массив ['x', 'x', 'x', 'x', 'x'].*/ 

function makeArray(value, length) { 
	let arr1 = [];
	for (i = 0; i < length; i++) {
		arr1.push(value);
	}
  console.log("task 14.3 :", arr1);
}

makeArray('x', 5);

//------------------------------------------------------------------------------------------------------------------

//4. Дан массив с числами. Узнайте сколько элементов с начала массива надо сложить, чтобы в сумме получилось больше 10-ти.

let arr4 = [ 1, 3, 5, 7, 9, 11, 13 ];

//------------------------------------------------------------------------------------------------------------------

//5. Дан массив с числами. Не используя метода reverse переверните его элементы в обратном порядке.

let A = [ 10, 20, 30, 40, 50, 60, 70 ];
// console.log("task 14.5.1 :", A);

// let n = A.length;
// let center = Math.ceil(n/2) - 1;

// for(i = 0; i <= center; i++){
//     z = A[i];
//     A[i] = A[n-1-i];
//     A[n-1-i]=z;
    
// }
function revArr() {
  let newArr = [];
   for(let i = A.length-1; i >= 0; i--) {
    newArr.push(A[i]);
  }
  return newArr;
};

console.log("task 14.5 :", revArr());

//------------------------------------------------------------------------------------------------------------------

/*6. Дан двухмерный массив с числами, например [[1, 2, 3], [4, 5], [6]]. 
Найдите сумму элементов этого массива. Массив, конечно же, может быть произвольным*/

let arr6 = [[1, 2, 3], [4, 5], [6]];

// function getSumElementsArray(){
//   arr66 = arr6.flat(Infinity);
//   let value = arr66.reduce((previousValue, item) => previousValue + item);
//   return value;
// }

// console.log("task 14.6 :", getSumElementsArray());

let sum6 = 0;
for(let i = 0; i < arr6.length; i++){
  for(let z = 0; z < arr6[i].length; z++){
    sum6 = sum6 + arr6[i][z];
  }
}
console.log("task 14.6 :", sum6);

//------------------------------------------------------------------------------------------------------------------

/* 7. Дан трехмерный массив с числами, например [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]. 
Найдите сумму элементов этого массива. Массив, конечно же, может быть произвольным.*/

let arr7 = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]];

// function getSumElementsArray2(){
//   arr77 = arr7.flat(Infinity);
//   let value = arr77.reduce((previousValue, item) => previousValue + item);
//   return value;
// }

// console.log("task 14.7 :", getSumElementsArray2());

let sum7 = 0;

for(let i7 = 0; i7 < arr7.length; i7++){
  for(let z7 = 0; z7 < arr7[i7].length; z7++){
    for(let x7 = 0; x7 < arr7[i7][z7].length; x7++){
      sum7 = sum7 + arr7[i7][z7][x7];
    }
  }
}
console.log("task 14.7 :", sum7);