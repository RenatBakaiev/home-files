//__________________________________________TASK 8_________________________________________________________________

//1. Даны два массива: [1, 2, 3] и [4, 5, 6]. Объедините их вместе

let arr11 = [1, 2, 3];
let arr12 = [4, 5, 6];

let arr13 = arr11.concat(arr12);

console.log("task 8.1 :", arr13);

//------------------------------------------------------------------------------------------------------------------

//2. Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1].

let arr2 = [1, 2, 3];

let arr21 = arr2.reverse();
console.log("task 8.2 :", arr21);

//------------------------------------------------------------------------------------------------------------------

//3. Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6.

let arr3 = [1, 2, 3];

arr3.push(4,5,6);
console.log("task 8.3 :", arr3);

//------------------------------------------------------------------------------------------------------------------

//4. Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6.

let arr4 = [1, 2, 3];
arr4.unshift(4,5,6);
console.log("task 8.4 :", arr4);

//------------------------------------------------------------------------------------------------------------------

//5. Дан массив ['js', 'css', 'jq']. Выведите на экран первый элемент.

let arr5 = ['js', 'css', 'jq'];

let res5 = arr5.shift();
console.log("task 8.5 :", res5);

//------------------------------------------------------------------------------------------------------------------

//6. Дан массив ['js', 'css', 'jq']. Выведите на экран последний элемент.

let arr6 = ['js', 'css', 'jq'];

let res6 = arr6.pop();
console.log("task 8.6 :", res6);

//------------------------------------------------------------------------------------------------------------------

//7. Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [1, 2, 3].

let arr7 = [1, 2, 3, 4, 5];

res7 = arr7.slice(0,3); // - ВОЗВРАЩАЕТ
console.log("task 8.7 :", res7);

//------------------------------------------------------------------------------------------------------------------

//8. Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [4, 5].

let arr8 = [1, 2, 3, 4, 5];

res8 = arr8.slice(3,5); // - ВОЗВРАЩАЕТ
console.log("task 8.8 :", res8);

//------------------------------------------------------------------------------------------------------------------

//9. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5].

let arr9 = [1, 2, 3, 4, 5];

arr9.splice(1,2); // - ВОЗВРАЩАЕТ
console.log("task 8.9 :", arr9);

//------------------------------------------------------------------------------------------------------------------

//10. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice запишите в новый массив элементы [2, 3, 4].

let arr10 = [1, 2, 3, 4, 5];

arr10.splice(0, 5, 2, 3, 4); // - ВОЗВРАЩАЕТ
console.log("task 8.10 :", arr10);

//------------------------------------------------------------------------------------------------------------------

//11. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 2, 3, 'a', 'b', 'c', 4, 5].

let arr111 = [1, 2, 3, 4, 5];

arr111.splice( 3, 0, 'a', 'b', 'c'); // - ВОЗВРАЩАЕТ
console.log("task 8.11 :", arr111);

//------------------------------------------------------------------------------------------------------------------

//12. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 'a', 'b', 2, 3, 4, 'c', 5, 'e'].

let arr122 = [1, 2, 3, 4, 5];

arr122.splice( 1, 0, 'a', 'b');
arr122.splice(6, 0, 'c');
arr122.splice(8, 0, 'e');

console.log("task 8.12 :", arr122);



//------------------------------------------------------------------------------------------------------------------

//13. Дан массив [3, 4, 1, 2, 7]. Отсортируйте его.

let arr133 = [3, 4, 1, 2, 7];

arr133.sort();
console.log("task 8.13 :", arr133);

//------------------------------------------------------------------------------------------------------------------

//14. Дан объект {js:'test', jq: 'hello', css: 'world'}. Получите массив его ключей.

let data = {
  js:'test', 
  jq: 'hello', 
  css: 'world',
};

let res14 = Object.keys(data);
console.log("task 8.14 :", res14);


