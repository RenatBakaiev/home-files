//__________________________________________TASK 19________________________________________

/*task 1.2 Повторите поведение кнопки по нажатию на нее (она меняет текст в инпуте): */

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	input.value = '!!!!!!';
// }

//------------------------------------------------------------------------------------------

/*task 1.3 Повторите поведение кнопки по нажатию 
на нее (она выводит алертом содержимое инпута): */

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	alert(input.value);
// };

//------------------------------------------------------------------------------------------

/*task 1.4 Повторите поведение кнопки по нажатию на нее 
(она выводит алертом содержимое инпута, возведенное в квадрат): */

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	var number = Number(input.value);
// 	var square = number*number;
// 	alert(square);
// };

//------------------------------------------------------------------------------------------

/*task 1.5 Повторите поведение кнопки по нажатию на нее 
(она осуществляет обмен содержимым между двумя инпутами,
можете понажимать на нее несколько раз или вручную сменить содержимое инпутов): */

function buttonClick() {
	var input1 = document.getElementById('input1');
	var input2 = document.getElementById('input2');
	var input1Value = input1.value;
	var input2Value = input2.value;
	input1.value = input2Value;
	input2.value = input1Value;
};

//------------------------------------------------------------------------------------------

/*task 1.6 Повторите поведение кнопки по нажатию на нее (поменяется ее текст) */

// function buttonClick(elem){
// 	elem.value = 'New text button';
// };

//------------------------------------------------------------------------------------------

/*task 1.7. Повторите поведение кнопки по нажатию на нее (она меняет цвет текста в инпуте): */

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	input.style.color = 'red';
// };

// function buttonClick() {
// 		document.getElementById('input').style.color = 'red';
// 	};

//------------------------------------------------------------------------------------------

/*task 1.8. Повторите поведение кнопок по нажатию на них 
		(одна из них блокирует инпут с помощью атрибута disabled, а другая разблокирует): */
		
//block

// function button1Click(){
// 	var input = document.getElementById('input');
// 	input.disabled = true;
// };

//unblock

// function button2Click(){
// 	var input = document.getElementById('input');
// 	input.disabled = false;
// };

//------------------------------------------------------------------------------------------

/* TASK 1.1 Повторите страницу по данному по образцу - при нажатии - сообщение:*/

// function buttonClick(){
// 	alert('Hello!');
// };

//------------------------------------------------------------------------------------------

/* TASK 1.3 Двойной щелчек - сообщение:*/

// function makeDC(){
// 	alert('Hello!');
// };

//------------------------------------------------------------------------------------------

// TASK 1.5 Кнопка - инпут - сообщение:

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	alert(input.value);
// };

//------------------------------------------------------------------------------------------

// TASK 1.6 Кнопка - меняет сообщение в инпуте:

// function buttonClick() {
// 	var input = document.getElementById('input');
// 	input.value = 'Changed text';
// };

//------------------------------------------------------------------------------------------

// TASK 1.7 Кнопка - меняет картинку:

// function buttonClick() {
// 	elem = document.getElementById('image1');
// 	elem.src = "pic/2.jpg";
// };

// function buttonClick2() {
// 	elem = document.getElementById('image1');
// 	elem.src = "pic/1.jpg";
// };

//------------------------------------------------------------------------------------------

// TASK 1.8 Инпут - клик - сообщение:

// function makeAlert(elem){
// 	console.log(elem.value);

// }

//------------------------------------------------------------------------------------------

// TASK 1.9 Инпут - клик - Change text:

// function changeText(elem){
// 	elem.value = "New text";
// };

//------------------------------------------------------------------------------------------

// TASK 1.10 ИИнпут - клик - Change text 2 раза::

// function changeText(elem){
// 	elem.value = "New text";
// };
// function changeTextSecondTime(elem){
// 	elem.value = "Super New text";
// };

//------------------------------------------------------------------------------------------

// TASK 1.11 Инпут submit - клик - Change text:

// function changeValue(elem){
// 	elem.value = 'Changed this value text';
// };

//------------------------------------------------------------------------------------------

// TASK 1.12 Нажал на Инпут submit Change text + кнопка заблокирована:

// function changeValue(elem){
// 	elem.value = 'You cant push me now!!!';
// 	elem.disabled = true; // делает elem неактивным
// };

//------------------------------------------------------------------------------------------

// TASK 1.13 Рисунок меняется при наведении (обезьяны):

// function over(elem){
// 	elem.src = 'pic/2.jpg' // обращаемся к elem через src 
// };
// function out(elem){
// 	elem.src = 'pic/1.jpg'
// };

//------------------------------------------------------------------------------------------

// TASK 1.14 Нажал на кнопку - поменял цвет текста в инпуте:

// function changeInput() {
// 		elem = document.getElementById('input') // сначала обратиться к элементу
// 		elem.style.fontSize = '35px';
// 		elem.style.fontWeight = 'bold';
// 		elem.style.color = 'red';
// 		elem.style.backgroundColor = 'green'; // потом менять стили
// 	};

//------------------------------------------------------------------------------------------

// TASK 1.15 Нажал на кнопку - скрыл/показал инпут:

// function hideInput(){
// 	elem = document.getElementById('input'); //сначала обратиться к элементу
// 	elem.style.display = 'none';	// потом менять стиль - скрыть
// }

// function showInput(){
// 	elem = document.getElementById('input'); //сначала обратиться к элементу
// 	elem.style.display = 'inline-block';
// }

//------------------------------------------------------------------------------------------

// TASK 1.16 Нажал на кнопку - поменял инпут:

// function changeInput(){ // функция в onclick
// 	elem = document.getElementById('input'); //сначала обратиться к элементу
// 	elem.value = 'Changed input'
// 	elem.style.fontWeight = 'bold';
// 	elem.style.fontSize = '35px';
// 	elem.style.borderRadius = '10px';
// 	elem.style.color = 'red';	// потом менять стили 
// }

//------------------------------------------------------------------------------------------

// /* ANN's TASK: Добавьте в html кнопку и пусть по клику на неё массив с именами, 
// из прошлых задачек перебирается и выводится как список*/

// let arrNames = ['Петя','Вася','Катя','Таня','Симка','Нолик']; // создаем массив

// function makeList(){ // функция из value onclick
// 	arrNames.forEach(function(item){ // перебираем массив
//         let li = document.createElement('li'); //создание li
//         li.innerText = item; // присвоение значения
//         list = document.querySelector("ol"); //обращение к ol
//         list.appendChild(li); // вставка li в ol       
// });
// };

//------------------------------------------------------------------------------------------

/* ANN's TASK2: Добавьте в html кнопку и пусть по клику на неё массив с именами, 
из прошлых задачек перебирается и выводится как список. 
Сделайте так, чтобы массив передавался как параметр функции и был динамическим*/

var arrNames = []; //пустой массив

function addValue(){ // функция из value onclick
	newName = document.getElementsByTagName("input")[0].value; //новое значение
	arrNames.push(newName); // добавить его в массив
	// inputDel = document.getElementsdById("del");
	// inputDel.value = "";
}
function makeList(){ // функция из value onclick
	arrNames.forEach(function(item){ // перебираем массив
        let li = document.createElement('li'); //создание li
        li.innerText = item; // присвоение значения
        list = document.querySelector("ol"); //обращение к ol
        list.appendChild(li); // вставка li в ol       
});
};