//__________________________________JSON________________________________________________

let listOfCountries = [
    {
        "id":1,
        "name":"Країни",
        "children":[
            {
                "id":11,
                "name":"СНД (склад починаючи з 2009 року)",
                "children":[
                    {
                        "id":111,
                        "name":"Митний Союз(склад починаючи з 2015 року)",
                        "children":[
                            {
                                "id":2,
                                "name":"Росія",
                            },
                            {
                                "id":21,
                                "name":"Вірменія",
                            },
                            {
                                "id":22,
                                "name":"Білорусь",
                            },
                            {
                                "id":23,
                                "name":"Казахстан",
                            },
                        ]
                    },
                    {
                        "id":222,
                        "name":"Інші країни СНД",
                        "children":[
                            {
                                "id":31,
                                "name":"Азербайджан",
                            },
                            {
                                "id":32,
                                "name":"Таджикистан",
                            },
                            {
                                "id":45,
                                "name":"Молдова",
                            }
                        ]
                    }
                ]
            },
            {
                "id":56,
                "name":"ЄС (склад починаючи з 2013 року)",
                "children":[
                    {
                        "id":58,
                        "name":"Австрія",
                    },
                    {
                        "id":70,
                        "name":"Бельгія",
                    },
                    {
                        "id":95,
                        "name":"Болгарія",
                    },
                    {
                        "id":94,
                        "name":"Кіпр",
                    },
                    {
                        "id":205,
                        "name":"Чехія",
                    }
                ]
            }
        ]
    },
]  

//--------------------------------------------------------------------------------------------

//work variant https://qna.habr.com/q/565599

//   function createTree(data) {
//     const ul = document.createElement('ul');
  
//     data.forEach(item => {
//       const li = document.createElement('li');
//       li.textContent = item.name;
  
//       if (item.children) {
//         li.appendChild(createTree(item.children));
//       }
  
//       ul.appendChild(li);
//     });
  
//     return ul;
//   }
  
  
//   document.querySelector('#mainContent').appendChild(createTree(listOfCountries));

//------------------------------------------------------------------------------------------

//work variant kate

let countries = document.querySelector('.countries'); // обратились к ul по классу

function makeListOfCountries(arr, list){ //вызываем функцию 
    arr.forEach(function(item){ //перебираем массив
        let li = document.createElement('li'); //создание li
        list.appendChild(li); // вставка li в ul
        li.innerText = item.name; // присвоение li значения ключа объекта

        let ul = document.createElement('ul'); //создали ul
        ul.id = item.id;  
        li.appendChild(ul);

        if (item.hasOwnProperty('children')){ // если у item (объекта) есть ключ children
            makeListOfCountries(item.children, ul); // вызывается функция перебора массива (children) 
        }
    });

};
makeListOfCountries(listOfCountries, countries);

// -----------------------------------------------------------------------------------------------------

//work variant ann

// let ul = document.createElement('ul');

// let main = document.getElementById('main');
// main.appendChild(ul);


// function makeTree(arr, parent) {
//     arr.forEach(item => {
//         let li = document.createElement('li');
//         li.innerText = item.name;
//         parent.appendChild(li);
        
        
//         if(item.children && item.children.length) {
//             let next_parent = document.createElement('ul');
//             li.appendChild(next_parent);
    
//             makeTree(item.children, next_parent);
//         }
//     });
// }

// makeTree(listOfCountries, ul);

//-----------------------------------------------------------------------------------------------------

//выбор всех li (querySelectorAll) в списке по id countries и перебор циклом for of

for ( let li of countries.querySelectorAll('li')){
    let span = document.createElement('span');             // создаем span 
    span.classList.add('show');                             // даем им class show 
    li.prepend(span);                                       // вставляем span перед li   
    span.append(span.nextSibling);                          // вставляем span после li
};

countries.onclick = function(event  ){
    // console.log(event.target.tagName);                       // при клике вывод в консоль
    if (event.target.tagName != 'SPAN') return;                // если клик не на теге span - выход
    let childrenContainer = event.target.parentNode.querySelector('ul');
    // при клике на span - получаем родителя ul 

    if(!childrenContainer) return; // проверка - если нету childrenContainer (вложеных детей  ) - выход

    childrenContainer.hidden = !childrenContainer.hidden; // hidden - отображения скрытности элемента

    if (childrenContainer.hidden){ // если элемент видимый
        event.target.classList.add('hide'); // Добавляем класс hide
        event.target.classList.remove('show'); // удаляем класс show
    }else{ // и наоборот иначе 
        event.target.classList.add('show'); // Добавляем класс show
        event.target.classList.remove('hide'); // удаляем класс hide
    }

}