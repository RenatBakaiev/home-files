//__________________________________________TASK 6_________________________________________________________________

//1. Даны переменные a = 10 и b = 3. Найдите остаток от деления a на b.

let a = 10;
let b = 3;

let c = a % b;
console.log("task 6.1 :",c);

//------------------------------------------------------------------------------------------------------------------

 /*2.Даны переменные a и b. Проверьте, что a делится без остатка на b. 
 Если это так - выведите 'Делится' и результат деления, иначе выведите 'Делится с остатком' и остаток от деления.*/

let d = 10;
let e = 3;

let res = d % e;
if (res != 0){
    console.log("task 6.2 :", 'Делится с остатком ' + res);
} else {
    console.log("task 6.2 :",'Делится, результат деления: ' + (d / e));
};

//------------------------------------------------------------------------------------------------------------------

//3. Возведите 2 в 10 степень. Результат запишите в переменную st.

let a1 = 2;
let b1 = 10;

let st = Math.pow(2, 10);
console.log("task 6.3 :", st);

//------------------------------------------------------------------------------------------------------------------

//4. Найдите квадратный корень из 245.

let a4 = 245;
let b4 = Math.sqrt(a4);
console.log("task 6.4 :", b4);

//------------------------------------------------------------------------------------------------------------------

/*5. Дан массив с элементами 4, 2, 5, 19, 13, 0, 10. 
Найдите квадратный корень из суммы кубов его элементов. Для решения воспользуйтесь циклом for.*/

let N = [4, 2, 5, 15, 13, 0, 11];

let changeArray = (a) => N.map(i => Math.pow(i, 3))
let R = changeArray(N)
// console.log(R)
let calc = (a) => R.reduce((previousValue, item) => previousValue + item);
let sum = calc(N);
let result = Math.sqrt(sum);

console.log("task 6.5 :", result);

//------------------------------------------------------------------------------------------------------------------

//6.Найдите квадратный корень из 379. Результат округлите до целых, до десятых, до сотых.

let a6 = 379;

let b6 = Math.sqrt(a6);

console.log("task 6.6.0 :", b6);
console.log("task 6.6.1 :", Math.round(b6));
console.log("task 6.6.2 :", b6.toFixed(1));
console.log("task 6.6.3 :", b6.toFixed(2));

//------------------------------------------------------------------------------------------------------------------

/*7.Найдите квадратный корень из 587. 
Округлите результат в большую и меньшую стороны, запишите результаты округления в объект с ключами 'floor' и 'ceil'.*/

let a7 = 587;

let b7 = Math.sqrt(a7);
console.log("task 6.7.0 :", b7);

let data = {
    floor: Math.floor(b7),
    ceil: Math.ceil(b7),
};

console.log("task 6.7 :", data);

//------------------------------------------------------------------------------------------------------------------

//8. Даны числа 4, -2, 5, 19, -130, 0, 10. Найдите минимальное и максимальное число.

let a8 = Math.max(4, -2, 5, 19, -130, 0, 10);
let b8 = Math.min(4, -2, 5, 19, -130, 0, 10);

console.log("task 6.8.1 :", a8);
console.log("task 6.8.2 :", b8);

//------------------------------------------------------------------------------------------------------------------

//9. Выведите на экран случайное целое число от 1 до 100.

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

console.log("task 6.9 :", getRandomInt(1, 100));

//------------------------------------------------------------------------------------------------------------------

//10. Заполните массив 10-ю случайными целыми числами.

let array = [];
let min10 = 1;
let max10 = 1000;

for (i = 0; i < 10; i++) {
	array[i] = Math.floor(Math.random() * (max10 - min10 + 1)) + min10;
}

console.log("task 6.10 :", array);

//------------------------------------------------------------------------------------------------------------------

//11. Даны переменные a и b. Найдите найдите модуль разности a и b. Проверьте работу скрипта самостоятельно для различных a и b.

let a11 = 500;
let b11 = 800;

let c11 = Math.abs(a11-b11);

console.log("task 6.11 :", c11);

/*12.  Даны переменные a и b. Найдите найдите модуль разности a и b. 
Проверьте работу скрипта самостоятельно для различных a и b.*/

let a12 = 3;
let b12 = 5;

let a121 = 6;
let b121 = 1;

let c12 = Math.abs(a12-b12);
let c121 = Math.abs(a121-b121);


console.log("task 6.12.1 :", c12);
console.log("task 6.12.2 :", c121);

//------------------------------------------------------------------------------------------------------------------

/*13. Дан массив arr. Найдите среднее арифметическое его элементов. 
Проверьте задачу на массиве с элементами 12, 15, 20, 25, 59, 79.*/

let array13 = [12, 15, 20, 25, 59, 79];

let value = array13.reduce((previousValue, item) => previousValue + item);
let resultat = value / array13.length;
console.log("task 6.13 :", resultat);

//------------------------------------------------------------------------------------------------------------------

/*14. Напишите скрипт, который будет находить факториал числа. Факториал (обозначается !) - 
это произведение (умножение) всех целых чисел, меньше данного, и его самого. Например, 4! = 1*2*3*4.*/



function factorial(n) {
    return (n != 1) ? n * factorial(n - 1) : 1;
  }

  console.log("task 6.14 :", factorial(5));

  //------------------------------------------------------------------------------------------------------------------