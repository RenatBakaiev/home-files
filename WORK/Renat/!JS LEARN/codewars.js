//__________________________________________CODEWARS_________________________________________________________________

/*1. Обычно, когда вы что-то покупаете, вас спрашивают, верен ли номер вашей кредитной карты, 
номер телефона или ответ на ваш самый секретный вопрос. Однако, поскольку кто-то может оглянуться через плечо, 
вы не хотите, чтобы это отображалось на экране. Вместо этого мы маскируем это.
Ваша задача - написать функцию maskify, которая изменяет все, кроме последних четырех символов '#'.*/

let str = '5168757346768387';
let str2 = str.slice(12,16);

function maskify(str){
   let strx = '############';
   return strx + str2;     
};

console.log(maskify());

//------------------------------------------------------------------------------------------------------------------

/*2. Есть массив с некоторыми числами. Все числа целые, кроме одного. Попробуйте найти это!*/

let arr2 = [ 0, 5, 0.55, 7, 1 ];

for(i = 0; i < arr2.length; i++){
    if (!Number.isInteger(arr2[i])){
    console.log(arr2[i]);
    }
};

//------------------------------------------------------------------------------------------------------------------

/*3. Создайте функцию, которая принимает число в качестве аргумента. Сложите все числа от 1 до числа, 
которое вы передали функции. Например, если вход равен 4, ваша функция должна вернуть 10, потому что 1 + 2 + 3 + 4 = 10.*/

function getSumOfNumbers(n3){
    let sum3 = 0;
    for(i = 0; i <= n3; i++){
        sum3 += i;
    }
    return sum3;
};

console.log(getSumOfNumbers(4));

//------------------------------------------------------------------------------------------------------------------

/*4. Вывести максимум score всех объектов*/

let arr4 = [
    { tile: "N", score: 10 },
    { tile: "K", score: 5 },
    { tile: "Z", score: 10 },
    { tile: "X", score: 8 },
    { tile: "D", score: 2 },
    { tile: "A", score: 1 },
    { tile: "E", score: 1 }
  ];

function getSumOfScores(){
    let sum4 = 0;
    for (i = 0; i < arr4.length; i++){
        sum4 += arr4[i].score;
    }
    return sum4;
};

console.log(getSumOfScores());

//------------------------------------------------------------------------------------------------------------------

/*5. Вашей функции будут переданы две функции, f и g, которые не принимают никаких параметров. 
Ваша функция должна вызывать их и возвращать строку, которая указывает, какая функция вернула большее число.*/

function f(){
    let sum51 = 0;
    for (i = 0; i < 10; i++){
        sum51 += i;
    }
    return sum51;
};

function g(){
    let sum52 = 0;
    for (i = 0; i < 15; i++){
        sum52 += i;
    }
    return sum52;
};

let f1 = f();
console.log(f1);

let g1 = g();
console.log(g1);

function getBiggestNumber(a,b){
    if (a > b){
        console.log('f');
    }else{
        console.log('g');
    }
};

getBiggestNumber(f1, g1); 

//------------------------------------------------------------------------------------------------------------------

/*6. Создайте функцию, которая принимает размеры аргумента объекта 
(содержит ключи ширины, длины, высоты) и возвращает объем поля.*/

let sizes = {
    width: 2,
    length: 5,
    height: 10 
}

function volumeOfBox(a, b, c){
    return a * b * c;
};

console.log(volumeOfBox(sizes.width, sizes.length, sizes.height));

//------------------------------------------------------------------------------------------------------------------

/*7. Предположим, у вас есть гостевой список студентов и страны их происхождения, 
которые хранятся в виде пар ключ-значение в объекте.
Напишите функцию, которая принимает имя и возвращает тег имени, который должен выглядеть так:
«Привет! Я [имя] и я из [страны]».
Если имя отсутствует в объекте, верните:
"Привет! Я гость."*/

let guest_list = {
    Randy: "Germany",
    Karla: "France",
    Wendy: "Japan",
    ''/*Koronavirus*/: "China",
    Norman: "England",
    Sam: "Argentina",
    
  };

  function sayNameCountry(){
      for(let key in guest_list){
        if(key === ''){
            console.log('Hi! I am quest' + ', I am from ' + guest_list[key]);
            continue;
        }
          console.log('Hi! I am ' + key  + ', I am from ' + guest_list[key]);
          
        }
    
};

  sayNameCountry();

//------------------------------------------------------------------------------------------------------------------

/*8.Создайте функцию, которая принимает объект в качестве аргумента и возвращает строку с фактами о городе. 
Факты о городе нужно будет извлечь из трех свойств объекта. Строка должна иметь следующий формат: 
X имеет население Y и находится в Z (где X - название города, Y - население, а Z - континент). город находится в).*/ 

let city1 = {
    name: "Paris",
    population: "2,140,526",
    continent: "Europe", 
};
let city2 = {
    name: "Tokyo",
    population: "13,929,286",
    continent: "Asia",
}

/*Методы объекта :*/

console.log(Object.keys(city1));      // - возвращает массив ключей
console.log(Object.values(city1));    // - возвращает массив значений
console.log(Object.entries(city1));   // - возвращает массив пар - [ключ, значение]

function getCityInfo(a8, b8, c8){
    return a8 + ' имеет население ' + b8 + ' и находится в ' + c8;
}

console.log(getCityInfo(city1.name, city1.population, city1.continent));
console.log(getCityInfo(city2.name, city2.population, city2.continent));

//------------------------------------------------------------------------------------------------------------------

/*9.Напишите функцию, которая преобразует объект в массив ключей и значений.*/

let object8 = {
    A: 1,
    B: 2,
    C: 3,
    D: 4,
    E: 5,
    F: 6,
    G: 7,
};

function makeArrayOfKeyAndValues(a){
    return Object.entries(a); 
}

console.log(makeArrayOfKeyAndValues(object8));

console.log(typeof(makeArrayOfKeyAndValues(object8)));
console.log(typeof(makeArrayOfKeyAndValues));

//------------------------------------------------------------------------------------------------------------------

/*10.Создайте функцию, которая принимает массив с объектами и возвращает сумму бюджетов людей.*/

let arr10 = [
    { name: "John", age: 21, budget: 23000 },
    { name: "Steve",  age: 32, budget: 40000 },
    { name: "Martin",  age: 16, budget: 2700 }
  ];
let arr11 = [
    { name: "Vasya", age: 33, budget: 65000 },
    { name: "Andrey",  age: 22, budget: 55000 },
    { name: "Kolja",  age: 21, budget: 45000 }
];


function getSumOfAllPeoplesBudgets(a){
    sum10 = 0;
    for(i = 0; i < a.length; i++){
        sum10 += a[i].budget;
    }
    return sum10;
}

console.log(getSumOfAllPeoplesBudgets(arr10));
console.log(getSumOfAllPeoplesBudgets(arr11));

//------------------------------------------------------------------------------------------------------------------

/*11.Предыдущий код выдает ошибку, потому что объект не был передан функции. Исправьте функцию, 
чтобы она возвращала размер по умолчанию, даже если в функцию ничего не передается. 
Не удаляйте объект {size = "big"} в параметре и не изменяйте оператор возврата. // big*/

function shirtSize({size = "big"} = 'big') {
    return size;
};
  
console.log(shirtSize());

//------------------------------------------------------------------------------------------------------------------

/*12. В этом испытании вам необходимо преобразовать вес, взвешенный на планете Солнечной системы, 
в соответствующий вес на другой планете. Чтобы преобразовать вес, 
вы должны разделить его на гравитационную силу планеты, на которой взвешивается, 
и умножить результат (массу) на гравитационную силу другой планеты. В таблице ниже приведен список сил гравитации: */

ThePowerOfGravity = {
    Mercury: 3.7,
    Venus: 8.87,
    Earth: 9.81,
    Mars: 3.711,
    Jupiter: 24.79,
    Saturn: 10.44,
    Uranus: 8.69,
    Neptune: 11.15,
};

let n = 100; // кг

function convertTheWeight(a12, b12, c12){
    result = b12 / a12 * c12;
    return result.toFixed(2);
}
console.log('Weight at Earth =', n, 'кг'); // Earth
console.log('Weight at Mars =',convertTheWeight(ThePowerOfGravity.Earth, n, ThePowerOfGravity.Mars), 'кг'); // Mars
console.log('Weight at Jupiter =',convertTheWeight(ThePowerOfGravity.Earth, n, ThePowerOfGravity.Jupiter), 'кг'); // Jupiter
console.log('Weight at Uranus =',convertTheWeight(ThePowerOfGravity.Earth, n, ThePowerOfGravity.Uranus), 'кг'); // Uranus

//------------------------------------------------------------------------------------------------------------------

/*13.Создайте функцию, которая определяет, соответствует ли заказ на покупку бесплатной доставке. 
Заказ имеет право на бесплатную доставку, если общая стоимость приобретенных товаров превышает 50,00 долларов США.*/

listOfBuys1 = {
  "Shampoo": 5.99,
  "Rubber Ducks": 15.99,  
};

listOfBuys2 = {
   "Flatscreen TV": 399.99, 
}

listOfBuys3 = {
    "Monopoly": 11.99,
    "Secret Hitler": 35.99,
    "Bananagrams": 13.99,
}

let n13 = 50;
let s13 = 15;

function checkingOfWayOfDelivery(a){
    sum13 = 0;
    for(let key in a){
        sum13 += a[key];
    }
    // sum13 > n13 ? 'Доставка безплатно' : 'Доставка платная';
    if (sum13 > n13){
        return 'Доставка безплатно';
    }else{
        return 'Доставка платная. Цена доставки: ' + s13 + ' $';
    }
    
}

console.log(checkingOfWayOfDelivery(listOfBuys1));

//------------------------------------------------------------------------------------------------------------------

/*14. 
Создайте функцию, которая принимает массив студентов и возвращает массив их главных заметок. 
Если у ученика нет заметок, давайте предположим, что его верхняя нота равна 0*/

let StudentNotes = [
    {
        id: 1,
        name: "Jacek",
        notes: [5, 3, 4, 2, 5, 5]
    },
    {
        id: 2,
        name: "Ewa",
        notes: [2, 3, 3, 3, 2, 5]
    },
    {
        id: 3,
        name: "Zygmunt",
        notes: [2, 2, 4, 4, 3, 3]
    }
];




  