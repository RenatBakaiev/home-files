//________________________________________________OBJECT_________________________________________________________________________________________________

/*1. Создайте объект city1 (var city1 = {}), +укажите у него свойства name 
(название города, строка) со значением «ГородN» и population (населенность города, число) со значением 10 млн.*/

  let city1 = {};
  city1.name = 'Город N';
  city1.population = 1e7;

  console.log("task object1 :", city1);

  //--------------------------------------------------------------------------------------------------------------------------------------------------------
  
  /*2. Создайте объект city2 через нотацию {name: "ГородM", population: 1e6}.*/

  let city2 = {
    name: 'Город М',
    population: 1e6,
  };

  console.log("task object2 :", city2);

//--------------------------------------------------------------------------------------------------------------------------------------------------------

/*3. Создайте у объектов city1 и city2 методы getName(), которые вернут соответствующие названия городов.*/

city1.getName = function(){
return this.name
};

console.log("task object3.1 :", city1.getName());

city2.getName = function(){
  return this.name
};
  
console.log("task object3.2 :", city2.getName());

//--------------------------------------------------------------------------------------------------------------------------------------------------------

/*4. Создайте методы exportStr() у каждого из объектов. Этот метод должен возвращать информацию о городе в формате 
«name=ГородN\npopulation=10000000\n».Для второго города будет строка со своими значениями. 
Примечание: можно обращаться к каждому свойству через цикл for/in, но методы объекта возвращать не нужно*/

city1.exportStr1 = function(){
console.log("task object4.1 :", 'name = ' + city1.name + '; population = ' + city1.population);
};

city1.exportStr1();

// city2.exportStr2 = function(){
//   console.log("task object4.2 :", 'name = ' + city2.name + '; population = ' + city2.population);
// };
  
// city2.exportStr2();

// city1.exportStr1 = function(){
//   for(key in city1)
//   console.log("task object4.1 :", 'name = ' + city1.name + '; population = ' + city1.population);
//   };
  
//   city1.exportStr1();

//--------------------------------------------------------------------------------------------------------------------------------------------------------

/*5. Создайте глобальную функцию getObj(), которая возвращает this. А у каждого из объектов city1 или city2 метод getCity, 
который ссылается на getObj. Проверьте работу метода. Примечание: к объекту вызова можно обратиться через this.*/

// let city51 = {
//   name: 'Город N',
//   population: 1e6,
//   getCity: function(){
//     return this.name
//   }
// };

// let city52 = {
//   name: 'Город M',
//   population: 1e5,
//   getCity: function(){
//     return this.name
//   }
// };

// console.log(city51.getCity());
// console.log(city52.getCity())

/*5. Создайте глобальную функцию getObj(), которая возвращает this. А у каждого из объектов city1 или city2 метод getCity, 
который ссылается на getObj. Проверьте работу метода. Примечание: к объекту вызова можно обратиться через this.*/

// function getObj1(){
//   console.log(this === window)
//   return this.city51.name;
// }
// function getObj2(){
//   console.log(city52.name);
// }

// let city51 = {
//     name: 'Город N',
//     population: 1e6,
//     getCity: function(){
//       getObj1();
//     }
//   };
  
// let city52 = {
//     name: 'Город M',
//     population: 1e5,
//     getCity: function(){
//       getObj2();
//     }
//   };
  
// city51.getCity();
// city52.getCity();

function getObj1(){
  return this.name;
}
function getObj2(){
 return this.name;
}

let city51 = {
    name: 'Город N',
    population: 1e6,
    getCity: getObj1
    
  };
  
let city52 = {
    name: 'Город M',
    population: 1e5,
    getCity: getObj2
  };
  
console.log(city51.getCity());
console.log(city52.getCity());