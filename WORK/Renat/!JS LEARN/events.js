//__________________________________________EVENTS_________________________________________________________________

/*1. Выберите section с идентификатором container без использования querySelector.*/

let elem1 = document.getElementById('container');

// console.log('EVENTS TASK 1: ',elem1);

elem1.style.color = 'green';

//------------------------------------------------------------------------------------------------------------------

/*2. Выберите section с идентификатором container использования querySelector.*/

let elem2 = document.querySelector('#container');

// console.log('EVENTS TASK 2: ',elem2);

//------------------------------------------------------------------------------------------------------------------

/*3. Выберите все элементы списка с классом «второй».*/

let elem3 = document.getElementsByClassName('second');

// console.log('EVENTS TASK 3: ',elem3);

//------------------------------------------------------------------------------------------------------------------

/*4.Выберите элемент списка с третьим классом, но только элемент списка внутри ol тега.*/

let elem4 = document.querySelector('ol .third');

// console.log('EVENTS TASK 4: ', elem4);

//---------------------------------------------------------------------------------------------------------

/*5. Дайте section с идентификатором container текста «Привет!».*/

// let hi = document.querySelector('#container');
// hi.innerText = 'Hello!';

// console.log('EVENTS TASK 5: ',elem2);
elem2.style.color = 'red';
// elem2.style.background = 'green';

//---------------------------------------------------------------------------------------------------------

/*6. Добавьте класс main к div классу footer.*/

let footer = document.querySelector('.footer');

footer.classList.add('main');

// console.log('EVENTS TASK 6: ',footer);

//---------------------------------------------------------------------------------------------------------

/*7. Удалить класс main на div с классом footer.*/

footer.classList.remove('main');

// console.log('EVENTS TASK 7: ',footer);

//---------------------------------------------------------------------------------------------------------

/*8. Создать новый li элемент*/

let newLi = document.createElement('li');
let newLi2 = document.createElement('li');

// console.log('EVENTS TASK 8: ', newLi);

//---------------------------------------------------------------------------------------------------------

/*9. Дайте li текст "четыре"*/

newLi.innerText = 'четыре';
newLi2.innerText = 'four';


// console.log('EVENTS TASK 9: ', newLi);

//---------------------------------------------------------------------------------------------------------

/*10. Добавьте li к ul элементу*/

let list = document.querySelector("ul");
list.appendChild(newLi);

let list2 = document.querySelector('ol');
list2.appendChild(newLi2);


// console.log('EVENTS TASK 10: ', );

//---------------------------------------------------------------------------------------------------------

/*11. Обведите все теги li внутри ol тега и присвойте им фоновый цвет «зеленый»*/

let liInsideOl = document.querySelectorAll('ol li');

for (let i = 0; i < liInsideOl.length; i++){
    liInsideOl[i].style.backgroundColor = 'green';
};

let liInsideUl = document.querySelectorAll ('ul li');

for (let i = 0; i < liInsideUl.length; i++){
    liInsideUl[i].style.backgroundColor = 'pink';
};
// console.log('EVENTS TASK 11: ', elem2);

//---------------------------------------------------------------------------------------------------------

/*12. Удалить div с классом footer*/

let footer2 = document.querySelector(".footer2");
footer2.remove();

let footer1 = document.querySelector(".footer");
footer1.remove();

//---------------------------------------------------------------------------------------------------------

let newDiv = document.createElement('div');
newDiv.innerText = 'This is my new Div';

let NewDivAdd = document.querySelector("div");
NewDivAdd.appendChild(newDiv);

newDiv.style.backgroundColor = 'blue';
newDiv.style.color = 'orange';



















 