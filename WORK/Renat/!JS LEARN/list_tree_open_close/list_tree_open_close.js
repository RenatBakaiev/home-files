//________________________________________JSON________________________________________________

let listOfCountries = [
    {
        "id":1,
        "name":"Країни",
        "children":[
            {
                "id":11,
                "name":"СНД (склад починаючи з 2009 року)",
                "children":[
                    {
                        "id":111,
                        "name":"Митний Союз(склад починаючи з 2015 року)",
                        "children":[
                            {
                                "id":2,
                                "name":"Росія",
                            },
                            {
                                "id":21,
                                "name":"Вірменія",
                            },
                            {
                                "id":22,
                                "name":"Білорусь",
                            },
                            {
                                "id":23,
                                "name":"Казахстан",
                            },
                        ]
                    },
                    {
                        "id":222,
                        "name":"Інші країни СНД",
                        "children":[
                            {
                                "id":31,
                                "name":"Азербайджан",
                            },
                            {
                                "id":32,
                                "name":"Таджикистан",
                            },
                            {
                                "id":45,
                                "name":"Молдова",
                            }
                        ]
                    }
                ]
            },
            {
                "id":56,
                "name":"ЄС (склад починаючи з 2013 року)",
                "children":[
                    {
                        "id":58,
                        "name":"Австрія",
                    },
                    {
                        "id":70,
                        "name":"Бельгія",
                    },
                    {
                        "id":95,
                        "name":"Болгарія",
                    },
                    {
                        "id":94,
                        "name":"Кіпр",
                    },
                    {
                        "id":205,
                        "name":"Чехія",
                    }
                ]
            }
        ]
    },
]  

//--------------------------------------------------------------------------------------------

// work variant I

// let ul = document.createElement('ul');

// let main = document.getElementById('main');
// main.appendChild(ul);


// function makeTree(arr, parent) {
//     arr.forEach(function(item) {
//         let li = document.createElement('li');
//         li.innerText = item.name;
//         parent.appendChild(li);

        
        // вторая задача сделать + -
        // if(item.children && item.children.length){                  // если есть элемент и он имеет детей
        //     let span = document.createElement('span');              // создаем span 
        //         // span.classList.add('hide');                         // даем им class hide 
        //         li.prepend(span);                                   // вставляем span перед li   
        //         span.append(span.nextSibling);                      // вставляем span после li
        //     let next_parent = document.createElement('ul');
        //         li.appendChild(next_parent);
        //         next_parent.hidden = true; // ul изначально скрытый
        //         makeTree(item.children, next_parent);               
        // }
     
        // вторая задача сделать + -
        // main.onclick = function(event){ // при нажатии на элемент с id main
        //         if (event.target.tagName != 'SPAN') return;     // если клик не на теге span - выход
        //         let childrenContainer = event.target.parentNode.querySelector('ul'); // childrenContainer = получаем родителя ul 
        //         if(!childrenContainer) return; // проверка - если нету childrenContainer (вложеных детей) - выход
        //             childrenContainer.hidden = !childrenContainer.hidden; // hidden - отображения скрытности/не скрытности элемента
                // if (childrenContainer.hidden){ // если элемент видимый
                //     event.target.classList.add('hide'); // Добавляем класс hide (классы отдельно в css)
                //     event.target.classList.remove('show'); // удаляем класс show
                // }else{ // и наоборот иначе 
                //     event.target.classList.add('show'); // Добавляем класс show
                //     event.target.classList.remove('hide'); // удаляем класс hide
                // }
            
//             };     
//     });
// };

// makeTree(listOfCountries, ul);

//------------------------------------------------------------------------------------------

// open/close elements - ЭТО ВАРИАНТ С ЮТУБА 

// 1. выбор всех li (querySelectorAll) в списке по id и перебор циклом for of (это мы сделали в строке 101)

// for ( let li of main.querySelectorAll('li')){
//     let span = document.createElement('span');             // создаем span 
//     span.classList.add('show');                             // даем им class show 
//     li.prepend(span);                                       // вставляем span перед li   
//     span.append(span.nextSibling);                          // вставляем span после li
// };

// main.onclick = function(event){
// //     // console.log(event.target.tagName);                       // при клике вывод в консоль
//     if (event.target.tagName != 'SPAN') return;                // если клик не на теге span - выход
//     let childrenContainer = event.target.parentNode.querySelector('ul');
// //     // при клике на span - получаем родителя ul 

// //     if(!childrenContainer) return; // проверка - если нету childrenContainer (вложеных детей) - выход

//     childrenContainer.hidden = !childrenContainer.hidden; // hidden - отображения скрытности элемента

//     if (childrenContainer.hidden){ // если элемент видимый
//         event.target.classList.add('hide'); // Добавляем класс hide
//         event.target.classList.remove('show'); // удаляем класс show
//     }else{ // и наоборот иначе 
//         event.target.classList.add('show'); // Добавляем класс show
//         event.target.classList.remove('hide'); // удаляем класс hide
//     }

// };

//------------------------------------------------------------------------------------------

// open/close elements

// for (let li of main.querySelectorAll('li')){
//     if(li.contains(li) == true){  
//     let span = document.createElement('span');             // создаем span 
//     span.classList.add('show');                             // даем им class show 
//     li.prepend(span);                                       // вставляем span перед li   
//     span.append(span.nextSibling);                          // вставляем span после li
//     }
// };



// main.onclick = function makeElemsOfTreeCloseOpen(event){ //при нажатии на элемент с id main выполняется функция
//     ul.classList.toggle('show');
// };




//--------------------------------------TOGGLE------------------------------------------------


let menuElem = document.getElementById('sweeties');
let titleElem = menuElem.querySelector('.title');

titleElem.onclick = function() {
  menuElem.classList.toggle('open');
};








//--------------------------------------TOGGLE------------------------------------------------

// test variant II

let ul = document.createElement('ul');

let main = document.getElementById('main');
main.appendChild(ul);


function makeTree(arr, parent) {
    arr.forEach(function(item) {
        let li = document.createElement('li');
        li.innerText = item.name;
        li.classList.add("hidden_li") // + - задача
        parent.appendChild(li);
        // li.innerHTML = '<span></span>';


        
        // вторая задача сделать + -
        if(item.children && item.children.length){                  // если есть элемент и он имеет детей
            let span = document.createElement('span');              // создаем span 
                li.prepend(span);                                   // вставляем span перед li   
                span.append(span.nextSibling);                      // вставляем span после li
                span.classList.add("span_c");
                // span.innerHTML = '+ ' + item.name; 
            let next_parent = document.createElement('ul');
                li.appendChild(next_parent);
                makeTree(item.children, next_parent);
                
                               
        };
     
        // вторая задача сделать + -

        // let liElem = document.getElementById('main'); // обращение по динамической id + ?       
        let liElem = document.querySelector('.hidden_li'); // обращение по динамической id + ?       
        let spanElem = liElem.querySelector('.span_c');
        spanElem.onclick = function (){
        liElem.classList.toggle('hidden_li');
        // spanElem.innerHTML = '- ' + item.name;
        
        };        
    });
};

makeTree(listOfCountries, ul);


