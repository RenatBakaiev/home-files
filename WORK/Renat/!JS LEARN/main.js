// JS
console.log("Let's start learn JS");
// Однострочный комментрарий
/*Многострочный 
    комментарий*/

//--------------------------------------------------------------------    

// alert('ПРИВЕТ JS');
// alert('learn JS, man');
// alert('МЫ БОГАТЫ!!!');

//--------------------------------------------------------------------

// let x = prompt('Tell me who you are:', 'default');  // ответ в строке - 'default'
// let y = prompt('Tell me who you are:');             // нету слова в строке по умолчанию
// let z = prompt();                                   // без текста

//--------------------------------------------------------------------

// console.log('HELLO'); // выводит значение в () в Сonsole

//--------------------------------------------------------------------

// confirm('question');                                   //окно с текстом ок или отмена
// confirm('Не желаешь получить бесплатный подарок?');    //окно с текстом ок или отмена
// let s = confirm('У вас сейчас погаснет экран!!!');

//--------------------------------------------------------------------

// let a = 42;
// let b = true;
// let c = undefined;
// let d = "Hello Man!";
// let e = null;
// let f = {
//     a: 12,
//     b: function f(){},
// };
// let g = Symbol();
// let h = function(){};


// console.log(typeof a);
// console.log(typeof b);
// console.log(typeof c);
// console.log(typeof d);
// console.log(typeof e);
// console.log(typeof f);
// console.log(typeof g);
// console.log(typeof h);

//--------------------------------------------------------------------


// let ex = false || '' || null;
// console.log(ex);

// let www = !false || 'hell/]' || null;
// let wwwee = 44 || 'yeah' || undefined || NaN;

// let wwweee = 414 || 'yeah' || undefined || NaN;
// let and = 414 && 'yeah' && 0;

// alert(www);

// alert(wwwee);

// alert('What a fuck?');
//  alert(and);

//---------------------------------ОБЛАСТИ ВИДИМОСТИ-----------------------

// function foo(a) {
// 	let b = a;
// 	return a + b;
// }

// let cc = foo( 2 );

// console.log(cc)

//-------------------------------

// function foo(a) {
// 	console.log( a + b );
// };

// let b = 2;


// let rr = foo(5);
// console.log(rr);

//-------------ВЛОЖЕННЫЕ ФУНКЦИИ-------------

// function foo(){
//     let a = 1;
//         function bar(){
//             let b = 2;
//                 function baz(){
//                     let c = 3;
//                     console.log(a+b+c);
//                 }
//                 baz();
//                 console.log(a+b);
//         }
//         bar();
//         console.log(a);
// }
// foo();

//--------------------------------------------

// setTimeout ( function () {
//     console.log('I waited 1 second!');
// }, 1000);

// let a = 2;

// //-------ФУНКЦИОНАЛЬНОЕ ВЫРАЖЕНИЕ---------

// (function foo(){

// 	let a = 3;
// 	console.log( a ); // 3

// })();

// console.log( a ); // 2

// var a = 30;
// var a = 20;
// console.log(a); // переприсвоится

// let b = 30;
// let b = 40; // ошибка
// console.log(b); 

//----------------МАССИВЫ----------------

// A = new Array(1,2,3,1986);
//
// console.log(A);
//
// B = [4,5,6,7,8,9];
//
// console.log(B);

//----------------МЕТОДЫ МАССИВОВ----------------


// A.push(88,77,66); 
// console.log(A);

// A.pop(66);
// console.log(A);

// C = [A.pop(66)];
// console.log(C);

// delete A[1];
// console.log(A);

// A.unshift('hello','hey');
// console.log(A);

// A.shift('hello');
// console.log(A);

// A.splice(0,4,'I','learn','JS');
// console.log(A);

//----------------МЕТОДЫ МАССИВОВ-11...12.02.20---------------

// let A = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "hello", 387, undefined, true, function(){}, {a:12, b:32}];
// let B = [1, 5, 88, 5, 7, 999, 888, 777, 555, 'do it', 'you', 'must', 'know', 'ALL'];


// console.log(A);
// console.log(B);

// 1 push

// A.push("elem", 'in', 'the', 'end');
// console.log(A);

// 2 slice

// let z = A.slice(0,10);
// console.log(z);

// map

// let N = z.map(item => item+5);
// console.log(N);

// let N1 = N.map(i => i * 2);
// console.log(N1);

// let N2 = N1.map(i => i-10);
// console.log(N2);


// sort
// let V = B.sort();
// console.log(V);
//

// filter

// let F = V.filter(item => typeof item == 'number');
// console.log(F);
//
// let D = V.filter(item => typeof item == 'string');
// console.log(D);
//
// let more = V.filter(item => item > 100);
// console.log(more);
//

//forEach

// F.forEach(function(item, pos, arr) {
//    console.log('Индекс ячейки:' + pos + ' Значение:' + item + ' Массив[' + arr + "]");
// });
//

// //slice

// let S = A.slice(0,10);
// console.log(S);
//
// let X = A.slice(-10);
// console.log(X);

//splice

// let C = A.splice(10, 6);
// console.log(C);
// let C1 = A.splice(10, 6,'new', 'elements');
// console.log(C1);
// console.log(A);
// A.splice(0, 10, 'i', 'deleted', 'numbers');
// console.log(A);

// find

// let EX =A.find(item => item > 100);
// console.log(EX);
// let EX =A.find(item => typeof item == 'string');
// console.log(EX);

// some - ечли хоть один из элементов удовлетворяет функции - то true иначе false

// res = A.some(item => item === undefined);
// console.log(res);
// numbers = A.some(item => typeof item == 'number');
// console.log(numbers);
// notNumber = A.some(item => item === NaN);
// console.log(notNumber);

// includes

// res = A.includes(9, 5);
// console.log(res);
//
// res = A.includes(undefined, 12); // 1 аргумент - то, что ищем, 2 аргумент - начиная с какого индекса
// console.log(res);

// reverse

// result = A.reverse();
// console.log(result);
//
// res = B.reverse();
// console.log(res);

// join

// res = A.join(); // массив превращается в строку
// console.log(res);

// let res = A.join('!'); // массив превращается в строку через разделитель !
// console.log(res);

// let res = B.join('@'); // массив превращается в строку через разделитель !
// console.log(res);

// concat

// let C = A.concat(B); // склеивание массивов
// console.log(A.concat(B));
//
// let D = A.concat('+','new', 'elements', ); // к массиву А добавить новые элементы
// console.log(D);

//reduce

// let example = [1, 2, 3, 4];
// console.log(example);
// let value = example.reduce((previousValue, item) => previousValue * item);
// console.log(value);


// let value = example.reduce(function(previousValue, item){
//     return previousValue * item
// });
// console.log(value);

// flat

// let arr = [1, 2, [3,4], ,5];
// let arr2 = arr.flat(Infinity);
// console.log(arr2);

// let arr = [1, 2, [[3,4],[5,5,5,[6, 6, {a:4, b:44},]] ,5]];
// let arr2 = arr.flat(Infinity);
// console.log(arr2);



//--------------------------------------------------------------------------------------------------------------


// function multiply(a, b){ // ------- зарегаться в codewars.comЫ
//     return a * b
//   }
// c = multiply(2,2);
// console.log(c);
// let name = 'list-style-image';
// let arr = name.split('-', 3);
// console.log(arr);

// let arr2 = arr.join('');
// console.log(arr2);

// console.log(typeof arr2);
// let C = A.concat(B)
// console.log(C);

// let sum = B.reduce(function(prev, item){
//     return prev+item;
// })
// console.log('Сумма элементов массива В =',sum);

// l = 2;
// var l;
// console.log(l); //2

// console.log(b); // undefined
// var b = 2;

// function foo() {
// 	let a = 2;

// 	function bar() {
// 		console.log( a );
// 	}

// 	return bar;
// }

// let baz = foo();

// baz();                  //-----------ЗАМЫКАНИЕ

// let highAndLow = '1 2 3 4 5';
//
// let arr = highAndLow.split(' ');
// console.log(arr);
//
// str = arr.join('');
// console.log(str);






//-----------------IF--------------------------

// let y = 50000;

// if (y == 1000){
//     console.log('крутая ЗП');
// }else if(y<1000){
//     console.log('bad');
// }else{
//     console.log('what?')
// };

//-----------------IF-------------------------

// let year = prompt('es6?');
// if (year == 2016){
//     console.log('cool!');
// }else{
//     console.log('no!BAD')
// }


// let age = prompt('Age: ');
// let access = age > 18 ? true : false;
// console.log(access);

//______________________________________FUNCTION_______________________________________________________

// let f = (a,b,c) => a+b+c;
// let a = f(1,2,3);
// console.log(a);


// let f = (a,b,c) => {
//     let res = a+b+c;
//     return res*10;
// };

// console.log(f(5,5,5));

//-------------------------------Умножить все элементы массива на 2 и перевернуть массив в 1 функции:

// let arr = [1, 2, 3, 4, 5];       // ОТДЕЛЬНО

// let F = arr.map(item => item*2); // ОТДЕЛЬНО

// console.log(F);

// let arr = [1, 2, 3, 4, 5];

// let changeArray = (a) => a.map(i => i*3).reverse();

//------------------OR----------------------------------

// let changeArray = (arr) => {
//     return arr.map(item => item*2).reverse();
//    }

// console.log('Решение: ',changeArray(arr));

//-------------------------------Отфильтровать друзей с длинной имени - 4буквы--------------------------------

// let friends = ["Ryan", "Kieran", "Jason", "Yous"];

// let filterFriends = (f) => f.filter(item => item.length === 4);

// console.log(filterFriends(friends));

//_____________________________________________________THIS____________________________________________________________

// let obj = {
//     x: 12,
//     mmm(){
//         console.log("this :", this.x);
//     }
// }
// obj.mmm();

// let obj2 = {
//     name: 'Vova',
//     getName: function(){
//         console.log(this.name);
//     }
// };
// obj2.getName();

// setTimeout(obj2.getName, 3000);


// let arr22 = [1, 2, 5];
// function getSum(a,b,c){
//     return a+b+c;
// };
// res = getSum.apply(window, arr22);
// console.log(res);

// for (let i = 0; i < 5; i++) {

//     if (i === 2) continue;
  
//     alert(i); 
//   }

  



//_____________________________________________________THIS____________________________________________________________

//_____________________________________________________OBJECT__________________________________________________________

// let objExample = {
//     a: 'hello',
//     b: 42,
//     'its string': true,
// };

// console.log('OBJECT EXAMPLE :', objExample.a);
// console.log('OBJECT EXAMPLE :', objExample['its string']);

// let user = new Object();
// console.log('OBJECT EXAMPLE :', user);

// user.a = 12;
// user.b = 13;
// user.c = 14;

// console.log('OBJECT EXAMPLE :', user);


// function Animal(type, legs) {  
//     this.type = type;
//     this.legs = legs;  
//     this.logInfo = function() {
//       console.log(this === myCat); // => true
//       console.log('The ' + this.type + ' has ' + this.legs + ' legs');
//     };
//   }
// let myCat = new Animal('Cat', 4);  
// setTimeout(myCat.logInfo.bind(myCat), 1000);

// function Period (hours, minutes) {  
//     this.hours = hours;
//     this.minutes = minutes;
//   }
//   Period.prototype.format = function() {  
//     console.log(this === walkPeriod); // => true
//     return this.hours + ' hours and ' + this.minutes + ' minutes';
//   };
//   var walkPeriod = new Period(2, 30);  

//   console.log(walkPeriod);

//_______________________________Recursion________________________________________________

// //Задача: выполнения вычисления a в степени b;
// function recursion(a,b){
//     // debugger;
//     if (b == 1){
//         return a;
//     }else{
//         return a * recursion(a, b - 1);
//     }
// };

// console.log('RECURSION :', recursion(3,4));

// // ИЛИ

// let recursion2 = (a,b) => b == 1 ? a : (a * recursion2(a, b-1));

// console.log('RECURSION ARROW FUNCTION :', recursion2(3,4));

//__________________________________________PROTOTYPE_____________________________________________

// let animal = {
//     eats: true,
//     kill: true,
//     die: true,
//     walk(){
//         console.log("it's walk");
//     },
// };

// let tiger = {
//     jumps: true,
// };

// tiger.__proto__ = animal;

// console.log(tiger.kill);
// console.log(tiger.walk());


//__________________________________________EVENT LOOP_________________________________________

// function main() {
    
//     console.log('A'); // 1. Сначала выводится 

//     setTimeout(function exec() {
//       console.log('EVENT LOOP: ', 'B'); // 3. Выводится в последнюю очередь после А и Сб
//     }, 0);                                      // когда call stack очистится
    
//     console.log('C'); // 2. Второй шаг


//     }

//     main();

//---------------------------------------------------------------------------------------------

//__________________________________________setTimeout_________________________________________

// function makeHi(a, b){
//     console.log( a + ',' + b );
// };

// setTimeout(makeHi, 5000, 'Hello', '4el'); // работа

// let timerId = setTimeout(makeHi, 5000, 'Hello', '4el');
// clearTimeout(timerId); // остановка

//__________________________________________setInterval_________________________________________

// function makeTick(){
//     console.log('tick');
// };

// setInterval(makeTick, 1000); // работа

// let timerId2 = setInterval(() => alert('tick'), 2000);

// setTimeout(() => {clearInterval(timerId2); alert('stop');}, 5000); // остановка


// 1. Напишите код для регистрации текущего времени каждые две секунды.
// function newDate(){
//     console.log('tick');
// };

// let timer = setInterval(newDate, 2000);

// 2. Напишите код для регистрации текущего времени каждые 2 секунды, пока не пройдет 30 секунд.
// setTimeout(() => {clearInterval(timer); alert('stop');}, 30000);

/* 3. Следующий код регистрирует каждую букву сразу. 
Измените это, чтобы регистрировать каждое письмо с задержкой в ​​1 секунду.*/

// let letters = ["a", "b", "c"]
// for (let l = 0; l < letters.length; l++) {
//   setTimeout(_ => {
//     console.log(letters[l])
//   }, l * 1000)
// };

//__________________________________________callback_________________________________________

// Пример колбэка мой

function makeCallBack(ac, callback){
    let res = ac * 2;
    console.log(res);
    callback();
};

makeCallBack(10, function (){
    console.log('its callback');
});

//--------------------------------------------------------------------------------------------

// function showCircle(cx, cy, radius, callback){
//     let div = document.createElement('div'); // создали div
//     div.style.width = 0; // ширина 0
//     div.style.height = 0; // высота 0
//     div.style.left = cx + 'px'; // координата x относительно окна браузера
//     div.style.top = cy + 'px'; // координата y относительно окна браузера
//     div.className = 'circle'; // класс дали
//     document.body.append(div); // вставили div в body
    
//     setTimeout(() => {
//         div.style.width = radius * 2 + 'px'; //задали новую высоту
//         div.style.height = radius * 2 + 'px'; //задали новую ширину
//         // callback(div);
//         div.addEventListener('transitionend', function handler(){ // добавление обработчика события
//             div.removeEventListener('transitionend', handler); // удаление обработчика события
//             callback(div); // колбек
//         });

//     });

// };

// function letsGo(){ //вызов функции с передачей аргументами значений  
//     showCircle(200, 200, 100, div => {
//         div.classList.add('message-ball'); // добавление нового класса
//         div.append('Hello');                // добавление текста
//     });
// };

//__________________________________________promice_________________________________________

// let promise = new Promise(function(resolve, reject) {
//     resolve('success');
  
//     setTimeout(() => resolve(2), 1000);
//   });
  
//   promise.then(alert); // выполнится только 'success' в resolve, resolve в setTimeout - игнорируется

//-------------------------------------------------------------------------------------------------------

//   let a = -5;
//   let www = new Promise(function(resolve, reject) {
//     if (a > 10){
//         resolve('success')
//     }else{
//         reject('error')
//     }
//   });

// console.log(www);

//-------------------------------------------------------------------------------------------------------

// let promise = new Promise(function(resolve, reject) {
//     setTimeout(() => resolve("done!"), 1000);
//   });
  
//   // resolve запустит первую функцию, переданную в .then
//   promise.then(
//     result => alert(result), // выведет "done!" через одну секунду
//     error => alert(error) // не будет запущена
//   );

//   let promise2 = new Promise((resolve, reject) => {
//     setTimeout(() => reject(new Error("Ошибка!")), 1000);
//   });
  
//   // .catch(f) это тоже самое, что promise.then(null, f)
//   promise2.catch(alert); // выведет "Error: Ошибка!" спустя одну секунду

//-------------------------------------------------------------------------------------------------------

// function f(){

//     f1();
//     f2();
//     function f1(){
//         debugger;
//     };
   
//     function f2(){
//         debugger;
//     };
// }

// f();

//-------------------------------------------------------------------------------------------------------

/*Встроенная функция setTimeout использует колбэк-функции. Создайте альтернативу, использующую промисы.
Функция delay(ms) должна возвращать промис, который перейдёт в состояние «выполнен» через ms миллисекунд,
так чтобы мы могли добавить к нему .then:*/

// function delay(ms){
//     return new Promise(resolve => setTimeout(resolve, ms));
// };

// delay(3000).then(() => alert('выполнилось через 3 секунды'));

//-------------------------------------------------------------------------------------------------------

/*Перепишите функцию showCircle, написанную в задании Анимация круга с помощью колбэка таким образом, 
чтобы она возвращала промис, вместо того чтобы принимать в аргументы функцию-callback.*/

function showCircle(cx, cy, radius){
    let div = document.createElement('div'); // создали div
    div.style.width = 0; // ширина 0
    div.style.height = 0; // высота 0
    div.style.left = cx + 'px'; // координата x относительно окна браузера
    div.style.top = cy + 'px'; // координата y относительно окна браузера
    div.className = 'circle'; // класс дали
    document.body.append(div); // вставили div в body
    

    return new Promise(resolve => {
        setTimeout(() => {
          div.style.width = radius * 2 + 'px';
          div.style.height = radius * 2 + 'px';
  
          div.addEventListener('transitionend', function handler() {
            div.removeEventListener('transitionend', handler);
            resolve(div);
          });
        }, 0);
      })
    }

/*Новое использование:*/


function letsGo(){ //вызов функции с передачей аргументами значений  
    showCircle(200, 200, 100).then(div => {
        div.classList.add('message-ball'); // добавление нового класса
        div.append('Hello');                // добавление текста
    });
};









// ____________________________________promice_front_end_pro____________________________

let userData = new Promise(function(resolve, reject){
    let x = Math.floor(Math.random()*10) - 5;
    setTimeout(function(){
        console.log('Timer 2 sec', x);
        if(x > 0){
            resolve(x);
        }
        reject(x)
    }, 2000)
});
console.log(userData);

userData
    .then(
        function(resp){
            console.log('THEN RESOLVE', resp);
        }, 
        function(error){
            console.log('THEN REJECT', error);
        }
    );

userData // два разных then абсолютно не связаны
    .then(function(resp){
            console.log(resp);
});

// AJAX with Promise

// let method = 'GET', path = '/info/user.json';

// let async = new Promise(function(){
//     var xhr = new XMLHttpRequest();
//     xhr.addEventListener('readystatechange', function(){
//         if(xhr.readyState === 4) {
//             // console.log(xhr)
//             success(xhr);
//         }
//     });
//     xhr.open(method, path, true);
//     xhr.send();
// });

// async
//     .then()
//     .then()
//     .then()
//     .catch()
