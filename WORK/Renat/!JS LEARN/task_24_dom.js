//__________________________________________TASK 24________________________________________

/*task 24.1 Дан элемент #elem. Добавьте ему класс www.*/

let class_test = document.querySelector('.class_test');

class_test.classList.add('www');

//------------------------------------------------------------------------------------------

/*task 24.2  Дан элемент #elem. Удалите у него класс www.*/

elem.classList.remove('www');

//------------------------------------------------------------------------------------------

/*task 24.3 Проверьте наличие у него класса www.*/

elem = document.getElementById('elem');

let checkClass = elem.classList.contains('www');
console.log(checkClass);

//------------------------------------------------------------------------------------------

/*task 24.4 Добавьте ему класс www, если его нет и удалите - если есть*/

checkClass == false ? elem.classList.add('www', 'mmm') : elem.classList.remove('www');

//------------------------------------------------------------------------------------------

/*task 24.5 Узнайте количество его классов.*/

elem = document.getElementById('elem');

let kolichestvo = elem.classList.length; //length количество классов
console.log(kolichestvo);

//------------------------------------------------------------------------------------------

/*task 24.6 Выведите последовательно алертом его классы.*/

elem = document.getElementById('elem');

classNames = elem.classList;

for (i = 0; i < classNames.length; i++){
	console.log(classNames[i]);
};

//------------------------------------------------------------------------------------------

/*task 24.7 Сделайте его красного цвета, размером 30px, добавьте ему границу. 
Решите задачу с помощью свойства cssText*/

elem = document.getElementById('elem');

elem.style.cssText = 'color: green; font-size: 30px; border: 1px solid blue;';

//------------------------------------------------------------------------------------------

/*24.8. По клику на него выведите название его тега*/

elem = document.getElementById('elem');

console.log(elem.tagName);

//------------------------------------------------------------------------------------------

/*24.9. По клику на него выведите название его тега в нижнем регистре*/

elem = document.getElementById('elem');
console.log(elem.tagName.toLowerCase());

//------------------------------------------------------------------------------------------

/*24.10. Добавьте каждому элементу в конец название его тега в нижнем регистре. */

elem = document.getElementById('elem');
elem2 = document.getElementById('elem2');

elem.classList.add(elem.tagName.toLowerCase());
elem2.classList.add(elem2.tagName.toLowerCase());

//------------------------------------------------------------------------------------------

/*24.11.  Дан ol. Вставьте ему в конец li с текстом 'пункт'.*/

let newLi = document.createElement('li'); // создали li
newLi.innerText = 'пункт'; // дали li текст


let parent = document.querySelector('ol');	// выбрали ol

parent.appendChild(newLi); // вставили li с текстом в ol

//------------------------------------------------------------------------------------------

/*24.12.  Дан ul. Дан массив. 
Вставьте элементы этого массива в конец ul так, чтобы каждый элемент стоял в своем li*/

let arr12 = ['i', 'want', 'to', 'be', 'a super', 'programmer'];

// присвоить ul id 'parent'

// запускаю цикл

var parent12 = document.querySelector('ul'); // обращаюсь к элементу parent через querySel 

for (var i = 0; i < arr12.length; i++){
	var newLi12 = document.createElement('li'); // создал li
	newLi12.innerText = arr12[i]; // наполняем каждый li текстом из массива
	parent12.appendChild(newLi12); // вставляю li в parent
	newLi12.addEventListener('click', makeMessage); // добавляем обработчика событий к элементу
	newLi12.setAttribute('id', 'before') // добавим каждому элементу id c value - before
};

//------------------------------------------------------------------------------------------

/*24.13. Сделайте так, чтобы к вставляемым li было привязано следующее событие: 
по нажатию на li она должна вывести на экран свой текст*/

// добавляем создание атрибутов всем li в цикле for выше

function makeMessage() {
	console.log(this.innerHTML)	
};

//------------------------------------------------------------------------------------------

/*24.14 Дан элемент ul, а в нем li #elem. 
Вставьте перед элементом #elem новую li с текстом ''My friend and '*/

newLi14 = document.createElement('li'); // создал li
newLi14.innerText = 'My friend and '; // наполняем  li текстом 

var parent14 = document.getElementById('parent');
var before = document.getElementById('before');

parent14.insertBefore(newLi14, before); // вставляем li после parent и до before

//------------------------------------------------------------------------------------------

/*15. Дан div. Вставьте перед ним span с текстом 'Its my new text'*/

span15_18 = document.createElement('span'); // создал span
span15_18.innerText = 'Its my new text'; // наполняем span текстом
 

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.insertAdjacentHTML('beforeBegin', '<span>Its my new text</span>'); // вставка перед div!


//------------------------------------------------------------------------------------------

/*16. Дан div Вставьте после него span с текстом 'Its my new text'*/

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.insertAdjacentHTML('afterEnd', '<span>Its my new text</span>'); //вставка после div!

//------------------------------------------------------------------------------------------

/*17. Дан div Вставьте ему в начало span с текстом 'Its my new text'*/

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.insertAdjacentHTML('afterBegin', '<span>Its my new text</span>'); 
//вставка внутри div! в начало 

//------------------------------------------------------------------------------------------

/*18. Дан div Вставьте ему в конец span с текстом 'Its my new text'*/

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.insertAdjacentHTML('beforeEnd', '<span>Its my new text</span>'); 
//вставка внутри div! в конец

//------------------------------------------------------------------------------------------

/*19. Дан div. Найдите первого потомка этого элемента и сделайте его текст каким-то цветом.*/

//берем div из предыдущего задания
var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.firstElementChild.style.color = 'red'; // делаем цвет первого красным

//------------------------------------------------------------------------------------------

/*20. Дан div. Найдите последнего потомка этого элемента и сделайте его текст каким-то цветом.*/

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id
my_div.lastElementChild.style.color = 'blue'; // делаем цвет первого синим

//------------------------------------------------------------------------------------------

/*21. Дан div. Найдите всех потомков этого элемента и добавьте им в конец текст 'endOfText'.*/

var my_div = document.getElementById('my_div_id'); // обращаемся к div через id

for (var i = 0; i < my_div.children.length; i++) { 
	my_div.children[i].innerHTML += ', --- end Of Text';
	}
// запускаем цикл и перебираем всех детей и добавляем в конец текст

//------------------------------------------------------------------------------------------

/*22. Дан p. Найдите его соседа сверху и добавьте ему в конец текст '!'*/

let my_p = document.getElementById('secoundElem'); // обращаемся к p через id
let neighbour_before = my_p.previousElementSibling; // определяем его соседа сверху
neighbour_before.innerHTML += '!' // +!

//------------------------------------------------------------------------------------------

/*23. Дан p. Найдите его соседа снизу и добавьте ему в конец текст '!'*/

let my_p2 = document.getElementById('secoundElem'); // обращаемся к p через id
let neighbour_after = my_p2.nextElementSibling; // определяем его соседа снизу
neighbour_after.innerHTML += '!' // +!

//------------------------------------------------------------------------------------------

/*24. Дан p. Найдите его соседа снизу его соседа снизу 
(следующий элемент за соседним) и добавьте ему в конец текст '!*/

let my_p22 = document.getElementById('secoundElem'); // обращаемся к p2 через id
my_p22.nextElementSibling.setAttribute('id', 'thirdElem') // присваиваем p3 id

let my_p3 = document.getElementById('thirdElem'); // обращаемся к p3 через id
let neighbour_after_after = my_p3.nextElementSibling; // определяем его соседа снизу
neighbour_after_after.innerHTML += '!' // +!

//------------------------------------------------------------------------------------------

/*25. Дан p. Найдите его родителя и покрасьте его в красный цвет.'!'*/

