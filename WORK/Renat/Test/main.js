// JS
console.log("Let's start learn JS");
// Однострочный комментрарий
/*Многострочный 
    комментарий*/

// var a = 33; //33 - life

// var amount = 99.99;
// {
//     amount = amount * 2;
//     console.log(amount);
// }
// var b = 100;
// const NALOG = 0.05;

// function Price(b){
//     b = b + (b + NALOG)
//    return b;
// }
// console.log(b*2);


// a = null;
// typeof a;

// console.log(typeof a);

// var obj = {
//     a: "hello",
//     b: 42,
//     c: true,
// }

// console.log(obj["a"]);
// console.log(obj.b);
//  var a = "hello";
//  var b = 3.14159;

// //  console.log(b.toFixed(4));
// var a = '42';
// var b = Number (a);

// var b = a * 1;


// console.log(a);



// function foo(){
//     a = 1;
// }
// foo();
// console.log(a);


// var a = 555;

// var b = (a > 41) ? "hello" : "world";


// console.log(b);



// function makeAdder(x) {
// 	// параметр `x` - внутренняя переменная

// 	// внутренняя функция `add()` использует `x`, поэтому
//     // у нее есть "замыкание" на нее
//     var x=2;
//     var y=3;
// 	function add(y) {
// 		return y + x;
// 	};

// 	return add;
// }

// z = makeAdder();

// console.log('Hello JS');

var a = 42;
var b = true;
var c = undefined;
var d = "Hello Man!";
var e = null;
var f = {
    a: 12,
    b: function f(){},
};
var g = Symbol();
var h = function(){};


console.log(typeof a);
console.log(typeof b);
console.log(typeof c);
console.log(typeof d);
console.log(typeof e);
console.log(typeof f);
console.log(typeof g);
console.log(typeof h);

const x = 10;
x.hello = 'hello';
console.log(x.hello);




