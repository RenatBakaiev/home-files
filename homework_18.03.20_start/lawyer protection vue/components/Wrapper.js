Vue.component('wrapper', {
    template: `
       <div class="wrapper">
        <div class="header_in">
            <div class="header_title">
                <div class="header_title_in">
                    <p class="header_title_in_text">главная</p>
                    <img src="pic/header_title_romb.svg" class="header_title_in_text_pic">
                    <p class="header_title_in_text">услуги</p>
                    <img src="pic/header_title_romb.svg" class="header_title_in_text_pic">
                    <p class="header_title_in_text">о адвокате</p>
                </div>
                <img src="pic/header_title_logo.svg" class="header_title_logo">
                <div class="header_title_in">
                    <p class="header_title_in_text">преимущества</p>
                    <img src="pic/header_title_romb.svg" class="header_title_in_text_pic">
                    <p class="header_title_in_text">сертификаты</p>
                    <img src="pic/header_title_romb.svg" class="header_title_in_text_pic">
                    <p class="header_title_in_text">контакты</p>
                </div>
            </div>
            <div class="lawyer_protection">
                <p class="lawyer_protection_text_title">Юридическая защита</p>
                <div class="lawyer_protection_svg_items">
                    <img src="pic/lawyar_protection_line.svg" class="lawyar_protection_svg_item">
                    <img src="pic/lawyar_protection_romb.svg" class="lawyar_protection_svg_item">
                    <img src="pic/lawyar_protection_line.svg" class="lawyar_protection_svg_item">
                </div>
                <p class="lawyer_protection_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar hendrerit sem, <br>
                    et pellentesque mi imperdiet eu. Fusce porttitor sollicitudin tincidunt.</p>
                <button onclick="leaveRequest()" class="lawyer_protection_button"><p class="lawyer_protection_button_text">Оставить заявку</p></button>
            </div>
            <img src="pic/header_end.svg" class="header_end_svg">
        </div>
    </div>
    `,
});